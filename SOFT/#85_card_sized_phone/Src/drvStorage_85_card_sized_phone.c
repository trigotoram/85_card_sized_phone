#include "drvStorage.h"
#include "board.h"
#include "_crc.h"
#include "halI2C.h"

void drvFRAM_init (void)
{
}

MSG drvFRAM_read (U16 adress, U8 *byte) // read FRAM
{
    U8 I2C_buf [4];
    
    I2C_buf [0] = adress >> 8; //adress 0x1FFF MAX
    I2C_buf [1] = adress & 0x00FF; //adress
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_FRAM_ADDRESS, &I2C_buf[0], 2, 0))
    {
        if (FUNCTION_RETURN_OK == halI2C_receive (I2C_FRAM_ADDRESS, &I2C_buf[0], 1, 0))
        {
            *byte = I2C_buf [0];
            return FUNCTION_RETURN_OK;
        }
    }
    return FUNCTION_RETURN_ERROR;
}

    
MSG drvFRAM_write (U16 adress, U8 byte) // write FRAM
{
    U8 I2C_buf [4];
    
    I2C_buf [0] = adress >> 8; //adress 0x1FFF MAX
    I2C_buf [1] = adress & 0x00FF; //adress
    I2C_buf [2] = byte;
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_FRAM_ADDRESS, &I2C_buf[0], 3, 0))
    {
        return FUNCTION_RETURN_OK;
    }
    return FUNCTION_RETURN_ERROR;
}


//�������-���������, ������ ������ �� FRAM, TODO ������� ��� � �� ��������� �����, ���� �� ���� 
//who cares...
MSG drvROM_write (U16 adress, U8 byte)
{
    return drvFRAM_write (adress, byte);
}

MSG drvROM_read (U16 adress, U8 *byte)
{
    return drvFRAM_read (adress, byte);
}

void drvROM_begin (U16 gg)
{
}

void drvROM_commit ()
{
}

void drvROM_end ()
{
}


EEPROM_config sEEPROM_config;
BOOL EEPROM_commit_flag;
U16 sEEPROM_config_CRC; //����������� �����


#define __DEBUG_EEPROM_TEST_ID          0

#if __DEBUG
u32 TEA_data[2] = { 0, 0 };
static const u32 TEA_key[4] PROGMEM = { 12332, 2348367676, 666, 474*33/332%33381271+3723888888*212663427/4*312166 };
#define ESP8266_ID_BUF_SIZE			1024
static const u32 ESP8266_IDs[1024 * 2] PROGMEM = //TODO some randomise data
{
	0x223978C2, 0xD183A84E, //0x00CAE38F - MY (+rtc)
	0xA40F42B1, 0xD1B01556, //0x00F367C0 - Oleg
	1, 5,
	0, 0,
	0xFFFFFFFF, 0xFFFFFFFF,
	//....

};
#endif


void drvStorage_init (void)
{
#if __DEBUG_EEPROM_TEST_ID
	//Serial.println(RANDOM_REG32);
	//Serial.print (F("ESP.getChipId :"));
	TEA_data[0] = 0xAA55AA55; // first must be 0xAA55AA55!
	TEA_data[1] = ESP.getChipId();
	Serial.printf(" ID this chip :0x%08X \n", ESP.getChipId());
	TEA_encrypt(TEA_data, (u32 *)TEA_key);
	Serial.printf("ID encrypt :0x%08X, 0x%08X\n", TEA_data[0], TEA_data[1]);
	//cheking id...
	bool test_id_passed = false;
	for (u32 i = 0; i < 1024; i++)
	{
		ESP.wdtFeed();
		TEA_data[0] = ESP8266_IDs[i * 2 + 0];
		TEA_data[1] = ESP8266_IDs[i * 2 + 1];
		//Serial.printf("data:0x%08X 0x%08X", TEA_data[0], TEA_data[1]);
		TEA_decrypt(TEA_data, (u32 *)TEA_key);
		//Serial.printf("ID decrypt :0x%08X 0x%08X\n", TEA_data[0], TEA_data[1]);
		if (ESP.getChipId() == TEA_data[1])
		{
			//Serial.print(F("passed on :"));
			//Serial.println(i);
			test_id_passed = true;
			break;
		}
	}
	if (true == test_id_passed)
	{
		Serial.println(F("ESP8266 ID test passed!"));
	}
#endif
    drvStorage_read ();
}


// set default settings
void drvStorage_restore (void)
{
	size_t EEPROM_conf_size = sizeof (sEEPROM_config);

	memset (&sEEPROM_config, '\0', EEPROM_conf_size);

	// wifi
	strcpy (sEEPROM_config.ssid, DEFAULT_SSID);
	strcpy (sEEPROM_config.pass, DEFAULT_PASS);
    
    sEEPROM_config.OLED_contrast = 1;

	drvStorage_save ();
}


// ���������� �������� �� EEPROM----------------------------------------
void drvStorage_read (void)
{
	U8 c;
	U16 i, crc_16;
	U16 EEPROM_conf_size = sizeof (sEEPROM_config);
	//EEPROM_conf_size = (EEPROM_conf_size / 2 + 1) * 2; //for real size

	memset (&sEEPROM_config, '\0', EEPROM_conf_size);
	drvROM_begin (HAL_EEPROM_SIZE);
    drvROM_read (0, &c);
	sEEPROM_config_CRC = (U16)c;
    drvROM_read (1, &c);
    sEEPROM_config_CRC = sEEPROM_config_CRC | (U16)c << 8;
#if _DEBUG_EEPROM
	U8 buf[] = { "123456789" }; //
	crc_16 = crc16_CITT(&buf[0], 9);
	Serial.printf("test CRC :%04X\n", crc_16);
	Serial.print(F("EEPROM read...\n"));
	Serial.printf("EEPROM_conf_size :%u\n", EEPROM_conf_size);
	for (i = 0; i < HAL_EEPROM_SIZE; i++)
	{
		c = EEPROM.read(i);
		Serial.printf("%02X[%c] ", c, c);
	}
#endif
	for (i = 0; i < EEPROM_conf_size; i++)
	{
		drvROM_read (i + 2, &c); //2 - crc size
		*((U8 *)&sEEPROM_config + i) = c;
	}
	crc_16 = crc16_CITT ((U8 *)&sEEPROM_config, EEPROM_conf_size); //check EEPROM crc
	drvROM_end();
#if _DEBUG_EEPROM
	Serial.print("\nCRC in EEPROM :");
	Serial.printf("%04X\n", sEEPROM_config_CRC);
	Serial.print("CRC must be :");
	Serial.printf("%04X\n", crc_16);
#endif
	if (sEEPROM_config_CRC != crc_16)
	{
#if _DEBUG_EEPROM
		Serial.println(F("Error CRC. Restore default settings"));
#endif
		drvStorage_restore();
		return;
	}
#if _DEBUG_EEPROM
	Serial.println(F("Successfull loading settings"));
#endif
}


// ������ �������� � EEPROM----------------------------------------
void drvStorage_save (void)
{
	U8 c;
	U16 i, crc_16;
    
	sEEPROM_config_CRC = 0;
	U16 EEPROM_conf_size = sizeof (sEEPROM_config);
	drvROM_begin (HAL_EEPROM_SIZE);
#if _DEBUG_EEPROM
	Serial.print(F("EEPROM save...\n"));
#endif
	for (i = 0; i < EEPROM_conf_size; i++)
	{
		c = *((U8 *)&sEEPROM_config + i);
		drvROM_write (i + 2, c); //2 - crc size
	}
    crc_16 = crc16_CITT ((U8 *)&sEEPROM_config, EEPROM_conf_size);
	sEEPROM_config_CRC = crc_16;
    drvROM_write (0, crc_16  & 0xFF);
	drvROM_write (1, crc_16 >> 8);
	drvROM_commit ();
	drvROM_end ();
#if _DEBUG_EEPROM
	Serial.println(F("\nSuccessful save settings"));
#endif
}


char *config_get_SSID(void)
{
	return sEEPROM_config.ssid;
}


char *config_get_PASS(void)
{
	return sEEPROM_config.pass;
}


    
    
    
#if BOARD_RS485_STTM

//#define __INLINE    


void halFLASH_unlock (void)
{  
    /* (1) Wait till no operation is on going */
    /* (2) Check that the Flash is unlocked */
    /* (3) Perform unlock sequence */
    while ((FLASH->SR & FLASH_SR_BSY) != 0) /* (1) */  
    {
        /* For robust implementation, add here time-out management */
    }

    if ((FLASH->CR & FLASH_CR_LOCK) != 0) /* (2) */
    {    
        FLASH->KEYR = FLASH_KEY1; /* (3) */
        FLASH->KEYR = FLASH_KEY2;
    }        
}

void halFLASH_lock (void)
{  
    /* (1) Wait till no operation is on going */
    /* (2) Check that the Flash is unlocked */
    /* (3) Perform unlock sequence */
    while ((FLASH->SR & FLASH_SR_BSY) != 0) /* (1) */  
    {
        /* For robust implementation, add here time-out management */
    }
    
    if ((FLASH->CR & FLASH_CR_LOCK) == 0) /* (2) */
    {    
        FLASH->KEYR = 0; /* (3) */
        FLASH->KEYR = 0;
    }
}


MSG halFLASH_erase_page (U32 page_addr)
{   
    MSG error;
    
    /* (1) Set the PER bit in the FLASH_CR register to enable page erasing */
    /* (2) Program the FLASH_AR register to select a page to erase */
    /* (3) Set the STRT bit in the FLASH_CR register to start the erasing */
    /* (4) Wait until the BSY bit is reset in the FLASH_SR register */
    /* (5) Check the EOP flag in the FLASH_SR register */
    /* (6) Clear EOP flag by software by writing EOP at 1 */
    /* (7) Reset the PER Bit to disable the page erase */
    FLASH->CR |= FLASH_CR_PER; /* (1) */    
    FLASH->AR =  page_addr; /* (2) */    
    FLASH->CR |= FLASH_CR_STRT; /* (3) */    
    while ((FLASH->SR & FLASH_SR_BSY) != 0) /* (4) */ 
    {
        /* For robust implementation, add here time-out management */
    }  
    if ((FLASH->SR & FLASH_SR_EOP) != 0)  /* (5) */
    {  
        FLASH->SR |= FLASH_SR_EOP; /* (6)*/
    }    
    /* Manage the error cases */
    else if ((FLASH->SR & FLASH_SR_WRPERR) != 0) /* Check Write protection error */
    {
        error |= ERROR_EEPROM_WRITE_PROTECTION; /* Report the error to the main progran */
        FLASH->SR |= FLASH_SR_WRPERR; /* Clear the flag by software by writing it at 1*/
    }
    else
    {
        error |= ERROR_UNKNOWN; /* Report the error to the main progran */
    }
    FLASH->CR &= ~FLASH_CR_PER; /* (7) */
    
    return error;
}


MSG halFLASH_erase_check (U32 first_page_addr)
{
    U32 i;  

    for (i = FLASH_PAGE_SIZE; i > 0; i-=4) /* Check the erasing of the page by reading all the page value */
    {
        if (*(U32 *)(first_page_addr + i -4) != (U32)0xFFFFFFFF) /* compare with erased value, all bits at1 */
        {
            return ERROR_EEPROM_ERASE; /* report the error to the main progran */
        }
    }
}


MSG halFLASH_FlashWord16Prog (U32 flash_addr, uint16_t data)
{    
    MSG error;
    /* (1) Set the PG bit in the FLASH_CR register to enable programming */
    /* (2) Perform the data write (half-word) at the desired address */
    /* (3) Wait until the BSY bit is reset in the FLASH_SR register */
    /* (4) Check the EOP flag in the FLASH_SR register */
    /* (5) clear it by software by writing it at 1 */
    /* (6) Reset the PG Bit to disable programming */
    FLASH->CR |= FLASH_CR_PG; /* (1) */
    *(__IO uint16_t*)(flash_addr) = data; /* (2) */
    while ((FLASH->SR & FLASH_SR_BSY) != 0) /* (3) */
    {
        /* For robust implementation, add here time-out management */
    }  
    if ((FLASH->SR & FLASH_SR_EOP) != 0)  /* (4) */
    {
        FLASH->SR |= FLASH_SR_EOP; /* (5) */
    }
    /* Manage the error cases */
    else if ((FLASH->SR & FLASH_SR_PGERR) != 0) /* Check Programming error */
    {      
        error = ERROR_EEPROM_PROG;
        FLASH->SR |= FLASH_SR_PGERR; /* Clear it by software by writing EOP at 1*/
    }
    else if ((FLASH->SR & FLASH_SR_WRPERR) != 0) /* Check write protection */
    {      
        error = ERROR_EEPROM_WRITE_PROTECTION; 
        FLASH->SR |= FLASH_SR_WRPERR; /* Clear it by software by writing it at 1*/
    }
    else
    {
        error = ERROR_UNKNOWN; 
    }
    FLASH->CR &= ~FLASH_CR_PG; /* (6) */
    
    return error;
}

#endif

void halFLASH_eraseAllPages (void)
{
    /*
    FLASH->CR |= FLASH_CR_MER; //������������� ��� �������� ���� �������
    FLASH->CR |= FLASH_CR_STRT; //������ ��������
//     while(!halFLASH_ready()){}; // �������� ����������.. ���� ��� ��� �������� �� � ���� �����...

//     FLASH->CR &= FLASH_CR_MER;
    */
}


void halFLASH_flashWrite (U32 address, uint8_t *data, U32 cnt)
{
/*    U32 i;

    while (FLASH->SR & FLASH_SR_BSY){}; //������� ���������� ����� � ������
    if (FLASH->SR & FLASH_SR_EOP)
    {
        FLASH->SR = FLASH_SR_EOP;
    }

    FLASH->CR |= FLASH_CR_PG; //��������� ���������������� �����

    for (i = 0; i < cnt; i += 2)
    {   //����� ������� 2 �����
        *(volatile uint16_t *)(address + i) = (((uint16_t)data[i + 1]) << 8) + data[i];
        while (!(FLASH->SR & FLASH_SR_EOP)){};
        FLASH->SR = FLASH_SR_EOP;
    }

    FLASH->CR &= ~(FLASH_CR_PG); //��������� ���������������� �����
    */
}


uint8_t halFLASH_read8 (U32 address)
{
    return (*(volatile uint8_t *) address);
}


uint16_t halFLASH_read16 (U32 address)
{
    return (*(volatile uint16_t *) address);
}


U32 halFLASH_read32 (U32 address)
{
    return (*(volatile U32 *) address);
}


void halFLASH_flashRead (U32 address, uint8_t *data, U32 cnt)
{
    U32 i;

    for (i = 0; i < cnt; i += 2)
    {
        data[i]     = halFLASH_read8 (address + i);
    }
}




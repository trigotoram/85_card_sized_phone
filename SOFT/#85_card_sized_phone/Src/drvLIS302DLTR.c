#include "board.h"
#include "drvLIS302DLTR.h"
#include "halI2C.h"


#if (I2C_LIS302DL == 1)

MSG    drvLIS3DH_get_reg( U8 adress, U8 *val)
{
	MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf [2];
    
    I2C_buf [0] = adress;
    if (FUNCTION_RETURN_OK == halI2C_transmit( I2C_LIS302DL_ADDRESS, &I2C_buf[0], 1, 0))
    {
        if (FUNCTION_RETURN_OK == halI2C_receive( I2C_LIS302DL_ADDRESS, &I2C_buf[0], 1, 0))
        {
            *val = I2C_buf [0];
            respond = FUNCTION_RETURN_OK;
        }
    }
    return respond;
}


MSG    drvLIS3DH_set_reg( U8 adress, U8 val)
{
	MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf [2];
    
    I2C_buf [0] = adress;
    I2C_buf [1] = val;
    if (FUNCTION_RETURN_OK == halI2C_transmit( I2C_LIS302DL_ADDRESS, &I2C_buf[0], 2, 0))
    {
        respond = FUNCTION_RETURN_OK;
    }
    return respond;
}


MSG    drvLIS302DL_getXYZ (U8 *x, U8 *y, U8 *z)
{
	MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf [8];
    
    I2C_buf[0] = (LIS302DL_REG_OUT_X | 0x80); // 0x80 for autoincrement
    if (FUNCTION_RETURN_OK == halI2C_transmit( I2C_LIS302DL_ADDRESS, &I2C_buf[0], 1, 0))
    {
        if (FUNCTION_RETURN_OK == halI2C_receive( I2C_LIS302DL_ADDRESS, &I2C_buf[0], 8, 0))
        {
            *x = I2C_buf[0];
            *y = I2C_buf[2];
            *z = I2C_buf[4];
            
            respond = FUNCTION_RETURN_OK;
        }
    }
    return respond;
}


MSG  drvLIS302DL_init (void)
{
	MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf [5];
    U8 tmpU8;
    
    I2C_buf[0] = LIS302DL_REG_WHOAMI;
    if (FUNCTION_RETURN_OK == drvLIS3DH_get_reg (LIS302DL_REG_WHOAMI, &tmpU8))
    {
        if (0x3B == tmpU8)
        {
            // enable all axes, 100Hz rate
            drvLIS3DH_set_reg (LIS302DL_REG_CTRL1, 0x07);
            
            drvLIS3DH_set_reg (LIS302DL_REG_CTRL2, 0x00);

            //Interrupt active - low, Push-pull/Open Drain selection on interrupt pad - open drain, Data Signal on Int1 pad - Click interrupt
            drvLIS3DH_set_reg (LIS302DL_REG_CTRL3, 0xC7);

            // enable all axes, normal mode // 100Hz rate
            drvLIS3DH_set_reg (LIS302DL_REG_CTRL1, 0x47);
            
            //single
            drvLIS3DH_set_reg (LIS302DL_REG_CLICKCFG, 0x55);
            //double
            //drvLIS3DH_set_reg (LIS302DL_REG_CLICKCFG, 0x6A);
            
            drvLIS3DH_set_reg (LIS302DL_REG_CLICK_THSY_X, 0x33);
            drvLIS3DH_set_reg (LIS302DL_REG_CLICK_THSZ, 0x03);
            drvLIS3DH_set_reg (LIS302DL_REG_TIMELIMIT, 0x03);
            drvLIS3DH_set_reg (LIS302DL_REG_TIMELATENCY, 0x33);
            drvLIS3DH_set_reg (LIS302DL_REG_TIMEWINDOW, 0x77);
            
            respond = FUNCTION_RETURN_OK;
        }
    }
    return respond;
}


U8  drvLIS302DL_get_click (void)
{
    U8 tmpU8 = 0;
    drvLIS3DH_get_reg (LIS302DL_REG_CLICKSRC, &tmpU8);
    return tmpU8;
}

#endif

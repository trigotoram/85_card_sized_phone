/**
  ******************************************************************************
  * File Name          : main.c
  * Description        : Main program body
  ******************************************************************************
  *
  * Copyright (c) 2018 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f0xx_hal.h"
#include "usb_device.h"

/* USER CODE BEGIN Includes */
#include "board.h"
#include "hal.h"
#include "modRTC.h"
#include "halI2C.h"
#include "drvLIS302DLTR.h"
#include "drvPCA8885.h"
#include "xprintf.h"
#include "_debug.h"
#include "modRandom.h"
#include "halPaint.h"
#include "modPaint.h"
#include "mod3D_func.h"
#include "halUSART.h"
#include "modSysClock.h"
#include "sysReport.h"
#include "conv.h"
#include "halKey.h"
#include "modKey.h"
#include "modMenu.h"
#include "drvStorage.h"
      
      
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
char _str[64];
U32 i, j;
U32 size;
//volatile static const U16 compile_date[]    = { 2018, 4, 3 };
volatile static const U16 compile_version[] = { 8 };
volatile static const U16 firmware_date[]   = { 2018, 4, 16 };
volatile static const char compile_date[12] = __DATE__;
volatile static const char compile_time[10] = __TIME__;
U32 testU32;
#define I2C_SEARCH_SIZE                 6
volatile U8 I2C_search_buf [I2C_SEARCH_SIZE];
volatile U8 I2C_search_cnt = 0;
volatile U8 I2C_search_adress = 0;
volatile U8 I2C_search_f = FALSE;
U8 I2C_buf[128];
U32 test_core_ticks_old;
U32 test_core_ticks_new;
U32 num_bytes;
U32 hal_error_cnt;
char tmp_char;
extern rtc_s mRTC;
extern EEPROM_config sEEPROM_config;
extern SYSTIME repaint_delay;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
void Error_Handler(void);
static void MX_GPIO_Init(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */



void error (void)
{
    static U32 errors = 0;
    
    errors++;
    while (1);
}



void debug_out (char * _str, U16 num_bytes)
{
    CDC_Transmit_FS (_str, num_bytes);
}

void show_message (const char *str)
{
}




extern MSG soft_halI2C_reinit (void);

extern void screensaver_mosaique_init (void);
extern void screensaver_mosaique_run (void);



void modSIM800L_send (const char *str)
{
    num_bytes = xsprintf (_str, "%s", str);
    halUSART3_sndM ((U8 *)_str, num_bytes );
    //_delay_ms(2000);
}



/* USER CODE END 0 */

int main(void)
{

  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration----------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* Configure the system clock */
  SystemClock_Config();

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USB_DEVICE_Init();

  /* USER CODE BEGIN 2 */
    //Freeze IWDG
    DBGMCU->APB1FZ |= DBGMCU_APB1_FZ_DBG_IWDG_STOP;
    //for Random
    _srand();
    _delay_init ();
    
    modSysClock_setRunPeriod (1000); // ����� ������� � �������
    //drvBUZZER_init ();
    
    //_debug_init ();
    //halUSB_pullup_ON ();
    
    halADC_init ();
    
    halSPI2_GPIO_SPI_init ();
    
    paint_init (SCREEN_ORIENTATION_180);
    hal_paint_picture ();
    hal_paint_update ();
    _delay_ms (100);
    paint_setBackgroundColor (COLOR_BLACK);
    paint_clearScreen ();
    paint_setColor (COLOR_WHITE);
    paint_setFont (PAINT_FONT_Generic_8pt, PAINT_FONT_MS);
    paint_putStrColRow (0, 0, "Start init...");
    hal_paint_update ();
    
    num_bytes = xsprintf (_str, "CDATE %s\r\n", compile_date);
    debug_out (_str, num_bytes);
    paint_putStrColRow (0, 1, _str);
    num_bytes = xsprintf (_str, "CTIME %s\n", compile_time);
    debug_out (_str, num_bytes);
    paint_putStrColRow (0, 2, _str);
    hal_paint_update ();
    // RTC------------------------------------------------------------
    if (modRTC_init () == 2)
    {
        hal_vibro_ON ();
        _delay_ms (200);
        hal_vibro_OFF ();
    }
    num_bytes = xsprintf (_str, "VER %u\n", BUILD_TIME);
    debug_out (_str, num_bytes);
    paint_putStrColRow (0, 3, _str);
    //num_bytes = xsprintf (_str, "DATE %04u.%02u.%02u\r\n", mRTC.year, mRTC.month, mRTC.day);
    //debug_out (_str, num_bytes);
    //paint_putStrColRow (0, 3, _str);
    num_bytes = xsprintf (_str, "TIME %02u:%02u:%02u\n", mRTC.hour, mRTC.minute, mRTC.second);
    debug_out (_str, num_bytes);
    paint_putStrColRow (0, 4, _str);
    hal_paint_update ();
    _delay_ms (30);
    
    // I2C test
    halI2C_init ();
    if (FUNCTION_RETURN_OK == soft_halI2C_reinit ())
    {
        num_bytes = xsprintf (_str, "I2C reinit!\r\n");
        debug_out (_str, num_bytes);
        paint_putStrColRow (0, 6, _str);
        hal_paint_update ();
    }

    while (1)
    {
        if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_search_adress, &I2C_buf[0], 0, 0))
        {
            I2C_search_buf [I2C_search_cnt] = I2C_search_adress;
            if (I2C_SEARCH_SIZE <= ++I2C_search_cnt)
            {
                I2C_search_f = TRUE;
                break;
            }
        }
        num_bytes = xsprintf (_str, "-%02X : %02X %02X %02X %02X \r\n", 
            I2C_search_adress,            
            I2C_search_buf[0], 
            I2C_search_buf[1],
            I2C_search_buf[2],
            I2C_search_buf[3]);
        debug_out (_str, num_bytes);
        I2C_search_adress += 2;
        if (I2C_search_adress >= 0x7E)
        {
            break;
        }
        _delay_us (10);
    }


    
    // LIS302 test
    if (FUNCTION_RETURN_OK == drvLIS302DL_init ())
	{
		num_bytes = xsprintf (_str, "drvLIS302DL_init OK\r\n");
	}
	else
	{
		num_bytes = xsprintf (_str, "drvLIS302DL_init ERROR!\r\n");
	}
    debug_out (_str, num_bytes);
    

    /*
    // read FRAM
    I2C_buf[0] = 0; //adress 0x1FFF MAX
    I2C_buf[1] = 0; //adress
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_FRAM_ADDRESS, &I2C_buf[0], 2, 0))
    {
        if (FUNCTION_RETURN_OK == halI2C_receive (I2C_FRAM_ADDRESS, &I2C_buf[0], 1, 0))
        {
            num_bytes = xsprintf (_str, "FRAM read %02X\r\n",            
                I2C_buf[0]);
            debug_out (_str, num_bytes);
        }
    }

    // write FRAM
    I2C_buf[2] = I2C_buf[0];
    I2C_buf[0] = 0; //adress 0x1FFF MAX
    I2C_buf[1] = 0; //adress
    I2C_buf[2]++;
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_FRAM_ADDRESS, &I2C_buf[0], 3, 0))
    {
        num_bytes = xsprintf (_str, "FRAM write %02X %02X %02X\r\n",            
            I2C_buf[0], 
            I2C_buf[1],
            I2C_buf[2]);
        debug_out (_str, num_bytes);
    }
    */
    //halUSART1_init (USART_BAUD_115200);
    
    
    
    //hal_ESP8266_ON ();
      
    //GSM module test ------------------
    ////////////////////////////////////////////
    // !!! Not ON test, if ANT not connected!!!
    ////////////////////////////////////////////
    /*
    GPIO_GSM_ON_H; //GSM on
    halUSART2_setBaud (USART_BAUD_9600); //GSM
    halUSART2_flush ();
    halUSART2_channel_select (UART2_SELECT_GSM);
    HAL_Delay (100);
    while (1)
    {
        size = xsprintf (_str, "%s", "AT\r\n"); //test
        halUSART2_sndM ((U8*)_str, size);
        HAL_Delay (100);
        if (FUNCTION_RETURN_OK == halUSART2_rcvM (GSM_buf, 8))
        {
            HAL_Delay (10);
            break;
        }
    }
    
    //HAL_Delay (10000);
    //size = xsprintf (_str, "%s", "ATA\r\n"); //D+0954494621;
    //halUSART2_sndM ((U8*)_str, size);
     //ATD89161234567;
    
    GPIO_GSM_ON_L; //GSM off
    */
    
    drvStorage_init ();
    contrast_set (sEEPROM_config.OLED_contrast);
    modKey_init ();
    
    
    halUSART3_init (9600);
    hal_SIM800_ONOFF ();
    
    modSIM800L_send("AT\r\n"); // ����������� �������� �����
    _delay_ms(2000);
    modSIM800L_send("ATE0\r\n"); // ��������� ��� � UART, ����� �� �������� ��� �������� ����� ������ �����������
    _delay_ms(2000);
    modSIM800L_send("AT+CHFA=0\r\n"); // �������� ����������
    _delay_ms(2000);
    modSIM800L_send("AT+CLVL=100\r\n"); // ������� �������� ��������
    _delay_ms(2000);
    modSIM800L_send("AT+CMIC=0,15\r\n"); // ���������������� ���������
    _delay_ms(2000);
    modSIM800L_send("AT+CSCS=\"GSM\"\r\n");
    _delay_ms(2000);
    modSIM800L_send("AT+CLIP=1\r\n"); // �������� ���
    _delay_ms(2000);
    modSIM800L_send("AT+CMGF=1\r\n"); // ��������� ����� (�� PDU)
    _delay_ms(2000);
    
    //ReadSIMBalance 
    //modSIM800L_send ("AT+CUSD=1,\"*#101#\"\r\n");
    //_delay_ms(8000);
    
    modSIM800L_send ( "AT+CMGS=\"+380954494621\"\r\n" );
    _delay_ms(200);
    modSIM800L_send ( "test for you, my love :3\r\n\r\n" );
    const char end_message [] = { 0x26, '\r', '\n', '\r', '\n' };
    modSIM800L_send ( end_message );
    _delay_ms(2000);
    
    paint_clearScreen ();
    paint_gotoColRow ( 0, 0 );
    
    halUSART3_flush ();
    
    while (1)
    {
        if (FUNCTION_RETURN_OK == halUSART3_rcvS ((U8 *)&tmp_char))
        {
            paint_putCharAuto ((char)tmp_char);
            if (tmp_char == 'R')
            {
                modSIM800L_send ( "ATA\r\n" );
            }
        }
        if (FUNCTION_RETURN_OK == modSysClock_timeout (&repaint_delay, 1000, SYSCLOCK_GET_TIME_MS_1))
        {
            hal_paint_update ();
            paint_putCharAuto ('.');
        }
    }
    
    
    
    menu_init (0);
    while (1)
    {
        halKey_run ();
        modKey_run ();
        menu_run ();
    }
    
    
    repaint_delay = 0;
    paint_clearScreen ();
    if ( drvMAX30100_init () == TRUE)
    {
        xsprintf (_str, "MAX30100 INIT!" );
        paint_putStrColRow (0, 0, _str); 
    }
    U32 temperature_cnt = 0;
    while (1)
    {
        if (FUNCTION_RETURN_OK == modSysClock_timeout (&repaint_delay, 30, SYSCLOCK_GET_TIME_MS_1))
        {
            U32 temperature = 0;
            temperature = (U32)drvMAX30100_retrieveTemperature() * 10000UL;
            xsprintf (_str, "%10u       ", temperature );
            paint_putStrColRow (0, 1, _str); 
            ++temperature_cnt;
            xsprintf (_str, "%10u       ", temperature_cnt );
            paint_putStrColRow (0, 2, _str); 
            hal_paint_update ();
        }
    }
    
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
  /* USER CODE END WHILE */

  /* USER CODE BEGIN 3 */
        //if (FUNCTION_RETURN_OK == halUSART1_rcvS (&tmp_char))
        {
            //halUSART1_sndS (tmp_char);
        }
      
        
        
        
        
        
        

        //num_bytes = xsprintf (_str, "Key: 0x%02X\r\n", key);
        //debug_out (_str, num_bytes);
                

        /*
        if (modKey_getState (KEY_LEFT) == MODKEY_STATE_PRESSED)
        {
            if (esp_on == TRUE)
            {
                hal_ESP8266_OFF ();
                esp_on = FALSE;
            }
            else
            {
                hal_ESP8266_ON ();
                esp_on = TRUE;
            }   
        }
        */
                
        
        
  }
  /* USER CODE END 3 */

}

/** System Clock Configuration
*/
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI48|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL3;
  RCC_OscInitStruct.PLL.PREDIV = RCC_PREDIV_DIV1;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USB;
  PeriphClkInit.UsbClockSelection = RCC_USBCLKSOURCE_HSI48;

  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void)
{

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();

}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
  while(1) 
  {
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef USE_FULL_ASSERT

/**
   * @brief Reports the name of the source file and the source line number
   * where the assert_param error has occurred.
   * @param file: pointer to the source file name
   * @param line: assert_param error line source number
   * @retval None
   */
void assert_failed(uint8_t* file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
    ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */

}

#endif

/**
  * @}
  */ 

/**
  * @}
*/ 

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

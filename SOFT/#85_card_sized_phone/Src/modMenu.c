#include "modMenu.h"
#include "modPaint.h"
#include "board.h"
#include "conv.h"
#include "xprintf.h"
#include "halPaint.h"
#include "modKey.h"
#include "modKey_local.h"
#include "modSysClock.h"
#include "hal.h"

// http://easyelectronics.ru/organizaciya-drevovidnogo-menyu.html



void dummy (void)
{
}


void dummyStr (char *pStr) //return zero string
{
    pStr[0] = END_CHAR;
}

menuItem_t	Null_Menu = {0, 0, 0, 0, 0, 0, 0}; // ������ �������, ����������
menu_t pMenu;
menuItem_t *selectedMenuItem; // ������� ����� ����

void menuChange (menuItem_t *NewMenu)
{
    if ((void *)NewMenu == (void *)&NULL_ENTRY)
    {
        return;
    }
    selectedMenuItem = NewMenu; // �������� ��������� ��
}

//U8 rrt = 0; // �������� ��� ����������



#define LED_ON
#define LED_OFF
BOOL led_state = FALSE;
void LED_set (S8 t)
{
    if (+1 == t)
        led_state = TRUE; // ��� �������� ������, �� 0 �� 255 ������������
    if (-1 == t)
        led_state = FALSE;
    //led_state ? LED_BLUE_ON : LED_BLUE_OFF;
}


void LED_get (char *str)
{
    xsprintf (str, "%s", led_state ? "On" : "Off");
}




void radio_mode_set (S8 t)
{
}

void radio_mode_get (char *str)
{
    xsprintf (str, "%d", 0); // %3u. frqA,
}


U32 test_cnt = 0;
BOOL esp_on = FALSE;
BOOL screensaver_on = FALSE;
extern rtc_s mRTC;
SYSTIME repaint_delay = 0;
SYSTIME rtc_delay = 0;
extern U32 num_bytes;
extern char _str[64];


void  sys_run (S8 t)
{
    paint_clearScreen ();
    while (1)
    {
        halKey_run ();
        modKey_run ();
        if (modKey_getState (KEY_0) == MODKEY_STATE_PRESSED)
        {
            drvBUZZER_peep (1);
            break;
        }
        if (modKey_getState (KEY_4) == MODKEY_STATE_PRESSED)
        {
            drvBUZZER_peep (3);
            hal_paint_deinit (0);
            /* Request to enter STANDBY mode */
            HAL_PWR_EnterSTANDBYMode();
        }
        
        if (FUNCTION_RETURN_OK == modSysClock_timeout (&rtc_delay, 50, SYSCLOCK_GET_TIME_MS_1))
        {
            modRTC_cnt_get (&mRTC);
            num_bytes = xsprintf (_str, "DATE %04u.%02u.%02u\r\n", mRTC.year, mRTC.month, mRTC.day);
            debug_out (_str, num_bytes);
            paint_putStrColRow (0, 0, _str);
            num_bytes = xsprintf (_str, "TIME %02u:%02u:%02u\n", mRTC.hour, mRTC.minute, mRTC.second);
            debug_out (_str, num_bytes);
            paint_putStrColRow (0, 1, _str);
            
            xsprintf (_str, "%4u mV      ", halADC_get_BATT_mV());
            paint_putStrColRow (0, paint_getMaxRow() - 1, _str); 
        }
            //drvBUZZER_peep (1);
            //_delay_ms (300);
            //paint_putStrColRow (0, 2, _str);
        //}
        if (FUNCTION_RETURN_OK == modSysClock_timeout (&repaint_delay, 10, SYSCLOCK_GET_TIME_MS_1))
        {
            if (esp_on != TRUE)
            {
                hal_paint_update ();
            }
        }
    }
}


void  screen_saver_mosaique_run (S8 t)
{
    paint_clearScreen ();
    screensaver_mosaique_init ();
    while (1)
    {
        halKey_run ();
        modKey_run ();
        if (modKey_getState (KEY_0) == MODKEY_STATE_PRESSED)
        {
            drvBUZZER_peep (1);
            break;
        }
        if (FUNCTION_RETURN_OK == modSysClock_timeout (&rtc_delay, 40, SYSCLOCK_GET_TIME_MS_1))
        {
            screensaver_mosaique_run ();
            hal_paint_update ();
        }
        if (FUNCTION_RETURN_OK == modSysClock_timeout (&repaint_delay, 10, SYSCLOCK_GET_TIME_MS_1))
        {
            if (esp_on != TRUE)
            {
                hal_paint_update ();
            }
        }
    }
}


void  screen_saver_life_run (S8 t)
{
    screen_saver_life_init ();
    while (1)
    {
        halKey_run ();
        modKey_run ();
        if (modKey_getState (KEY_0) == MODKEY_STATE_PRESSED)
        {
            drvBUZZER_peep (1);
            break;
        }
        if (modKey_getState (KEY_5) == MODKEY_STATE_PRESSED)
        {
            screen_saver_life_init ();
            drvBUZZER_peep (1);
        }
        if (FUNCTION_RETURN_OK == modSysClock_timeout (&repaint_delay, 10, SYSCLOCK_GET_TIME_MS_1))
        {
            screensaver_life_draw ();
            if (++test_cnt > 10000)
            {
                screen_saver_life_init ();
            }
        }
    }
}



void  screen_saver_3D_run (S8 t)
{
    mod3D_init ();
    hal_paint_update ();
    while (1)
    {
        halKey_run ();
        modKey_run ();
        if (modKey_getState (KEY_0) == MODKEY_STATE_PRESSED)
        {
            drvBUZZER_peep (1);
            break;
        }
        if (FUNCTION_RETURN_OK == modSysClock_timeout (&repaint_delay, 1, SYSCLOCK_GET_TIME_MS_1))
        {
            mod3D_run ();
            hal_paint_update ();
        }
    }
}


BOOL WIFI_status = FALSE;
void WIFI_set (S8 t)
{
    if (+1 == t)
    {
        WIFI_status = TRUE;
        hal_ESP8266_ON ();
    }
    if (-1 == t)
    {
        WIFI_status = FALSE;
        hal_ESP8266_OFF ();
    }
    
}


void WIFI_get (char *str)
{
    xsprintf (str, "%u", WIFI_status ? 1 : 0);
}


#include "modMenu_struct.h"

//---------------------------------------------------------
void  menu_setStyle (U8 st)
{
    switch (st)
    {
    case MENU_STYLE_NOSELECT:
        pMenu.style.bColor      = COLOR_BLACK;
        pMenu.style.borderColor = COLOR_WHITE;
        pMenu.style.fontColor   = COLOR_WHITE;
        break;

    case MENU_STYLE_SELECT:
        pMenu.style.bColor      = COLOR_BLACK;
        pMenu.style.borderColor = COLOR_BLACK;
        pMenu.style.fontColor   = COLOR_WHITE;
        break;
    }
    paint_setBackgroundColor (pMenu.style.bColor);
    paint_setColor (pMenu.style.fontColor);
}


typedef enum {
    MENU_STATE_IDLE = 0,
    MENU_STATE_CHECK,
    MENU_STATE_REDACT,

} tMENU_ENUM;


// �������������� ���� (�������-�� � �� �����)
void  menu_redrawBorders (void)
{ // border
    //U16 i;
    const char strMain[] = {"[Menu]"};

    paint_setBackgroundColor (pMenu.style.bColor);
    paint_clearScreen ();
//    menu_setStyle (MENU_STYLE_BORDER);
//    paint_setColor (pMenu.style.borderColor);
//    for (i = 1; i < paint_getMaxCol() -1; i++) { // Up
//        paint_putCharColRow (i, 0, '#');//128);
//    }
//    for (i = 1; i < paint_getMaxCol() -1; i++) { // Down
//        paint_putCharColRow (i, paint_getMaxRow() -1, '#');//128);
//    }
//    for (i = 1; i < paint_getMaxRow() -1; i++) { // Right
//        paint_putCharColRow (0, i, '#');//129);
//    }
//    for (i = 1; i < paint_getMaxRow() -1; i++) { // Left
//        paint_putCharColRow (paint_getMaxCol() -1, i, '#');//129);
//    }
//    paint_putCharColRow (0, 0, '#'); //130); //
//    paint_putCharColRow (0, paint_getMaxRow() -1, '#'); //131); //
//    paint_putCharColRow (paint_getMaxCol() -1, 0, '#'); //132); //
//    paint_putCharColRow (paint_getMaxCol() -1, paint_getMaxRow() -1, '#'); //133); //
//    // ������ ����
    paint_setColor (pMenu.style.fontColor);
    menu_redrawHeader (&strMain[0]);
}

// �������������� ������� ��������� ����???
void menu_redrawHeader (const char *str)
{
    //U8 size = sizeof(str) - 1; // @todo
    //paint_putStrColRow ((((paint_getMaxCol() - 1) / 2) - (size / 1)) - 0, 0, (char *)&str[0]);
}


// �������������� �������� ����
MSG   menu_setMenu (S8 menuPos, const char *str)
{
    S16 i, j;
    char _str[64];
    U16 posDraw = 0; //  ������� ��������� ������ � ���
    menuItem_t *tmpMenu;

    if ((void *)selectedMenuItem == (void *)&NULL_ENTRY)
        return FUNCTION_RETURN_ERROR; // imposible state

    paint_clearScreen ();
    for (j = -(S16)(paint_getMaxRow() -1); j < (S16)(paint_getMaxRow() -1); j++)
    {
        i = j;
        tmpMenu = selectedMenuItem;
        if (i > 0)
        {
            while (i != 0)
            {
                if ((void *)tmpMenu != (void *)&NULL_ENTRY)
                {
                    tmpMenu = (menuItem_t *)(tmpMenu->next);
                }
                i--;
            }
        }
        else
        {
            while (i != 0)
            {
                if ((void *)tmpMenu != (void *)&NULL_ENTRY)
                {
                    tmpMenu = (menuItem_t *)(tmpMenu->previous);
                }
                i++;
            }
        }
        if ((void *)tmpMenu == (void *)&NULL_ENTRY)
        { // ����� �� ����
            //NULL
        }
        else
        {
            if (j == 0) // highlight on
            {
                menu_setStyle (MENU_STYLE_SELECT);
                paint_putStrColRow (0, posDraw, ">");
            }
            else
            {
                menu_setStyle (MENU_STYLE_NOSELECT);
                paint_putStrColRow (0, posDraw, " ");
            }

            if ((void *)&dummyStr != (void *)tmpMenu->value)
            {
                char strValue[32];
                void  (*pf)(char *);
                pf = (void(*)(char *))tmpMenu->value;
                (*pf)(strValue);
                i = xsprintf (_str, "%s:%s", (char *)tmpMenu->text, strValue); // ����������� ����������
            }
            else
            {
                i = xsprintf (_str, "%s", (char *)tmpMenu->text);
            }
            paint_putStrColRow (1, posDraw, _str);
            i++;
            // clear string
            for (; i < (S16)(paint_getMaxRow() -1); i++)
            {
                paint_putCharColRow (i, posDraw, ' ');
            }
            posDraw++;
        }
    }
    
    hal_paint_update ();

    drvBUZZER_peep (1);
    
    return FUNCTION_RETURN_OK;
}


//
void    menu_init (U8 stileNum)
{
    paint_setBackgroundColor (COLOR_BLACK);
    paint_clearScreen ();
    hal_paint_update ();
    paint_setFont (PAINT_FONT_Generic_8pt, PAINT_FONT_MS);
    pMenu.state = MENU_STATE_CHECK;
    pMenu.position = 1;
    selectedMenuItem = (menuItem_t *)&MENU_P1;
    pMenu.drawPos  = pMenu.position;
    pMenu.change = TRUE;
}


MSG  menu_run (void)
{
    MSG respond = FALSE;
//screen_saver_life_run (0);
    switch (pMenu.state)
    {
    case MENU_STATE_CHECK:
        if (modKey_getState (KEY_8) == MODKEY_STATE_PRESSED) //KEY_UP
        {
            menuChange (PREVIOUS);
            pMenu.change = TRUE;
        }
        if (modKey_getState (KEY_2) == MODKEY_STATE_PRESSED) //KEY_DOWN
        {
            menuChange (NEXT);
            pMenu.change = TRUE;
        }
        if (modKey_getState (KEY_5) == MODKEY_STATE_PRESSED)
        {
            //menuChange (CHILD);
            //pMenu.change = TRUE;
            //if (&MENU_EXIT == selectedMenuItem) // ����� ��������\need exit
            //    respond = TRUE;
            //void  (*pf)(S8);
            //pf = (void(*)(S8))selectedMenuItem->callback;
            void  (*pf)(S8);
            pf = (void(*)(S8))selectedMenuItem->callback;
            if ((void(*)(S8))&dummy != (void(*)(S8))pf)
            {
                menuChange (PARENT);
            }
            else
            {
                //menuChange (CHILD);
            }
            pMenu.change = TRUE;
        }
        if (modKey_getState (KEY_4) == MODKEY_STATE_PRESSED) //KEY_LEFT
        {
            void  (*pf)(S8); //������� ��������� �� �������
            pf = (void(*)(S8))selectedMenuItem->callback; //�������� ���
            if ((void(*)(S8))&dummy != (void(*)(S8))pf)
            {
                (*pf)(-1); //��������� � ����������
            }
            else
            {
                menuChange (PARENT);
            }
            pMenu.change = TRUE;
        }
        if (modKey_getState (KEY_6) == MODKEY_STATE_PRESSED) //KEY_RIGHT
        {
            void  (*pf)(S8);
            pf = (void(*)(S8))selectedMenuItem->callback;
            if ((void(*)(S8))&dummy != (void(*)(S8))pf)
            {
                (*pf)(+1);
            }
            else
            {
                menuChange (CHILD);
            }
            pMenu.change = TRUE;
            //pMenu.key_hold_time = modSysClock_getTime ();
        }
        /*
        if (modKey_getState (KEY_RIGHT) == MODKEY_STATE_HOLD)
        {
            if (modSysClock_getPastTime (pMenu.key_hold_time, SYSCLOCK_GET_TIME_MS_1) > 500)
            {
                pMenu.key_hold_time = modSysClock_getTime ();
                {
                void  (*pf)(S8);
                pf = (void(*)(S8))selectedMenuItem->callback;
                (*pf)(+10);
                }
            }
        }
        */
        break;
    }
    /***/

    if (FALSE != pMenu.change)
    {
        //menu_redrawBorders ();
        menu_setMenu (0, "1234"); // ����� �������� ����
        
        pMenu.change = FALSE;
    }

//    paint.setColor(STYLE.fontColor);
//    if (1 == CURSOR.cursor) // ������ �������� :)
//    {
//        CURSOR.cursor = 0;
//        paint.putCharXY( CURSOR.x, CURSOR.y, ' ');
//    }
//    else
//    {
//        CURSOR.cursor = 1;
//        paint.putCharXY( CURSOR.x, CURSOR.y, '-');
//    }

    return respond;
}

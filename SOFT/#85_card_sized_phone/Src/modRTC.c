#include "modRTC.h"
#include "board.h"


#if MOD_RTC
extern rtc_t RTC_cnt; // ������� ������


#if RTC_CORRECT_TIC
#define SEC_ERROR                       ( 3650UL - 365UL )  // ������ ������ � ���
static U8  correctZnak = 0; // 0 ���� ������, 1 ���� �������

U32 correct = 0;
static U8 needCorrect = 0; // ���� ������������� ���������
#define CORRECT_CONSTANT                ((24UL*60UL*60UL*365UL) / SEC_ERROR) 
#endif


void modRTC_set (rtc_s *ftime)
{
    S32  a; // U8
    S32  y; // uS16
    S32  m; // U8
    rtc_t    JDN;
    
    // ���������� ����������� �������������
    a = (14 - ftime->month) / 12;
    y = (U32)ftime->year + 4800 - a;
    m = ftime->month + (12 * a) - 3;
    // ��������� �������� �������� ���������� ���
    JDN = ftime->day;
    JDN += (153 * m + 2) / 5;
    JDN += 365 * (U32)y;
    JDN += y / 4;
    JDN += -y / 100;
    JDN += y / 400;
    JDN = JDN - 32045; // ���������� � ������
    JDN = JDN - JD0; // ��� ��� ������� � ��� �����������, ������ ��� ������� ������ �� 01 ��� 2001 
    JDN *= 86400;     // ��������� ��� � �������
    JDN += ((U32)ftime->hour * 3600); // � ��������� ��� ��������� �������� ���
    JDN += ((U32)ftime->minute * 60);
    JDN += (ftime->second);
    // ����� ����� ���������� ������ � 00-00 01 ��� 2001
    //__DI();
    RTC_cnt = JDN;
    //__EI();
    //tmr_MillSecond = ftime->millisecond;
}


void modRTC_get (rtc_s *ftime)
{
    rtc_t    ace;
    S32  b; // U8
    S32  d; // U8
    S32  m; // U8
    
    //__DI();
    ace = RTC_cnt;
    //__EI();
    ace = (ace / 86400) + 32044 + JD0;
    b   = (4 * ace + 3) / 146097; // ����� �� ��������� ������ �������� ��-�� ������������ ?
    ace = ace - ((146097 * b) / 4); // �� � ��� ��������
    d   = (4 * ace + 3) / 1461;
    ace = ace - ((1461 * d) / 4);
    m   = (5 * ace + 2) / 153;
    ftime->day    = (U8)(ace - ((153 * m + 2) / 5) + 1);
    ftime->month  = m + 3 - (12 * (m / 10));
    ftime->year   = (U16)(100 * b + d - 4800 + (m / 10));
    ftime->hour   = (RTC_cnt / 3600) % 24;
    ftime->minute = (RTC_cnt / 60) % 60;
    ftime->second = (RTC_cnt % 60);
    
    //ftime->millisecond = tmr_MillSecond;
}


#if RTC_NEED_CALENDAR
U8 modRTC_get_weekday (rtc_s *ftime)
{
    S16 m, y;
    S16 c, weekday;

    m = ftime->month;
    y = ftime->year;
    if (m > 2) m -= 2; else m += 10, y--;
    c = y / 100;
    y %= 100;
    weekday = (ftime->day + (13 * m - 1) / 5 + y + y / 4 - c * 2 + c / 4) % 7;
    if (weekday < 0) weekday += 7;
    ftime->weekday = weekday;
    
    return (U8)(weekday);
}
#endif

volatile U16 tmr_MillSecond = 0; // ������� �����������



void  modRTC_run (void)
{
#if RTC_CORRECT_TIC
    tmr_MillSecond++;
    if (TIC_PERIOD <= tmr_MillSecond)
    {
        tmr_MillSecond = 0; 
        RTC_cnt++; 
    }
    
        // ���������
//     correct += CORRECT_CONSTANT;
//     if ( 1.0 <= correct)
//     {
//         correct = 0.0;
//         if (0 == correctZnak) // ������ ��������
//         {
//             TimerMillSecond--;
//         }
//         else
//         {
//             TimerMillSecond++;
//         }
//     }
    
    correct++; //
    if (CORRECT_CONSTANT <= correct)
    {
        correct = 0;
        needCorrect = 1;
    }
    if ((1 == needCorrect) && ((2 <= tmr_MillSecond) && (998 >= tmr_MillSecond))) // ��� ����� ��������, ��������� �� ������ ���� ������ 4-�
    {
        needCorrect = 0;
        if (0 == correctZnak) // ������ ��������
        {
            tmr_MillSecond--;
        } else {
            tmr_MillSecond++;
        }
    } 
#else
    //tmr_MillSecond++;
    //if (TIC_PERIOD <= tmr_MillSecond)
    {
        //tmr_MillSecond = 0;
        RTC_cnt++;
    }
#endif // RTC_CORRECT_TIC
}




#if RTC_NEED_ALARM
//MSG RTC_alarm_set (rtc_s *fbud) // & armed
//{ 
//    MSG respond = FUNCTION_RETURN_ERROR;
//    //ftime->second = sec;
//    return respond;
//}

MSG RTC_alarm_check (rtc_s *ftime, rtc_s *fbud)
{
    MSG respond = FUNCTION_RETURN_ERROR;

    if (//(ftime->year   == fbud->year)   &&
        //(ftime->month  == fbud->month)  &&
        //(ftime->day    == fbud->day)    &&
        (ftime->hour   == fbud->hour)   &&
        (ftime->minute == fbud->minute)// && 
        //(ftime->second >= fbud->second) && // @todo
        //(ftime->second < (fbud->second + 2))
    )
    {
        respond = FUNCTION_RETURN_OK;
    }
    return respond;
}
#endif // RTC_NEED_ALARM

/*
* �� ������������ ���� ����� ��������� ���:
* https://ru.m.wikipedia.org/wiki/%D0%AE%D0%BB%D0%B8%D0%B0%D0%BD%D1%81%D0%BA%D0%B0%D1%8F_%D0%B4%D0%B0%D1%82%D0%B0
*/
 
// (UnixTime = 00:00:00 01.01.1970 = JD0 = 2440588)
#define JULIAN_DATE_BASE    2440588
 /*
typedef struct
{
    U8 RTC_Hours;
    U8 RTC_Minutes;
    U8 RTC_Seconds;
    U8 RTC_Date;
    U8 RTC_Wday;
    U8 RTC_Month;
    U16 RTC_Year;
} RTC_DateTimeTypeDef;
 
// Get current date
void RTC_GetDateTime (U32 RTC_Counter, RTC_DateTimeTypeDef* RTC_DateTimeStruct)
{
    U32 time;
    U32 t1, a, b, c, d, e, m;
    int year = 0;
    int mon = 0;
    int wday = 0;
    int mday = 0;
    int hour = 0;
    int min = 0;
    int sec = 0;
    U64 jd = 0;;
    U64 jdn = 0;
 
    jd = ((RTC_Counter+43200)/(86400>>1)) + (2440587<<1) + 1;
    jdn = jd>>1;
 
    time = RTC_Counter;
    t1 = time/60;
    sec = time - t1*60;
 
    time = t1;
    t1 = time/60;
    min = time - t1*60;
 
    time = t1;
    t1 = time/24;
    hour = time - t1*24;
 
    wday = jdn%7;
 
    a = jdn + 32044;
    b = (4*a+3)/146097;
    c = a - (146097*b)/4;
    d = (4*c+3)/1461;
    e = c - (1461*d)/4;
    m = (5*e+2)/153;
    mday = e - (153*m+2)/5 + 1;
    mon = m + 3 - 12*(m/10);
    year = 100*b + d - 4800 + (m/10);
 
    RTC_DateTimeStruct->RTC_Year = year;
    RTC_DateTimeStruct->RTC_Month = mon;
    RTC_DateTimeStruct->RTC_Date = mday;
    RTC_DateTimeStruct->RTC_Hours = hour;
    RTC_DateTimeStruct->RTC_Minutes = min;
    RTC_DateTimeStruct->RTC_Seconds = sec;
    RTC_DateTimeStruct->RTC_Wday = wday;
}
 
// Convert Date to Counter
U32 RTC_GetRTC_Counter (RTC_DateTimeTypeDef* RTC_DateTimeStruct)
{
    U8 a;
    U16 y;
    U8 m;
    U32 JDN;
 
    a=(14-RTC_DateTimeStruct->RTC_Month)/12;
    y=RTC_DateTimeStruct->RTC_Year+4800-a;
    m=RTC_DateTimeStruct->RTC_Month+(12*a)-3;
 
    JDN=RTC_DateTimeStruct->RTC_Date;
    JDN+=(153*m+2)/5;
    JDN+=365*y;
    JDN+=y/4;
    JDN+=-y/100;
    JDN+=y/400;
    JDN = JDN -32045;
    JDN = JDN - JULIAN_DATE_BASE;
    JDN*=86400;
    JDN+=(RTC_DateTimeStruct->RTC_Hours*3600);
    JDN+=(RTC_DateTimeStruct->RTC_Minutes*60);
    JDN+=(RTC_DateTimeStruct->RTC_Seconds);
 
    return JDN;
}
 
void RTC_GetMyFormat(RTC_DateTimeTypeDef* RTC_DateTimeStruct, char * buffer) {
    const char WDAY0[] = "Monday";
    const char WDAY1[] = "Tuesday";
    const char WDAY2[] = "Wednesday";
    const char WDAY3[] = "Thursday";
    const char WDAY4[] = "Friday";
    const char WDAY5[] = "Saturday";
    const char WDAY6[] = "Sunday";
    const char * WDAY[7]={WDAY0, WDAY1, WDAY2, WDAY3, WDAY4, WDAY5, WDAY6};
 
    const char MONTH1[] = "January";
    const char MONTH2[] = "February";
    const char MONTH3[] = "March";
    const char MONTH4[] = "April";
    const char MONTH5[] = "May";
    const char MONTH6[] = "June";
    const char MONTH7[] = "July";
    const char MONTH8[] = "August";
    const char MONTH9[] = "September";
    const char MONTH10[] = "October";
    const char MONTH11[] = "November";
    const char MONTH12[] = "December";
    const char * MONTH[12]={MONTH1, MONTH2, MONTH3, MONTH4, MONTH5, MONTH6, MONTH7, MONTH8, MONTH9, MONTH10, MONTH11, MONTH12};
 
    sprintf(buffer, "%s %d %s %04d",
            WDAY[RTC_DateTimeStruct->RTC_Wday],
            RTC_DateTimeStruct->RTC_Date,
            MONTH[RTC_DateTimeStruct->RTC_Month -1],
            RTC_DateTimeStruct->RTC_Year);
}
*/

#endif //MOD_RTC

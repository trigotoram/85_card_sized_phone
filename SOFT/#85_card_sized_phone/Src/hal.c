#include "board.h"
#include "modSysClock.h"
#include "modRTC.h"
#include "drvStorage.h"

extern EEPROM_config sEEPROM_config;


void drvBUZZER_init (void)
{
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    GPIOB->MODER &= ~GPIO_MODER_MODER3;
    GPIOB->MODER |= GPIO_MODER_MODER3_1;
    GPIOB->OTYPER &= ~GPIO_OTYPER_OT_3;
    GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR3;
    GPIOB->PUPDR &= ~GPIO_PUPDR_PUPDR3;
    GPIOB->AFR[0] &= ~GPIO_AFRL_AFRL3;
    GPIOB->AFR[0] |= (2 << (3*4)); //AF2

    RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;
    
    TIM2->CR1 = 0;
    
    // ����������� ���������
    TIM2->PSC = (SYS_FREQ / 1000000UL) - 1;

    // ����������� ������ ������� = 1000 ������
    TIM2->ARR = 1000 - 1;

    // ����������� ���������� = 200 ������
    TIM2->CCR2 = 500;
    
    // ��������� 1 ����� � ����� ���2
    TIM2->CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2M_0;
    TIM2->CR1 |= TIM_CR1_ARPE; //autoreload

    // ����������� ������ �� ������������� 1 ������
    TIM2->CCER |= TIM_CCER_CC2E;
}


void drvBUZZER_peep (U8 numbers)
{
    while (numbers)
    {
        TIM2->CR1 |= TIM_CR1_CEN; //drvBUZZER_on (); //end init pika
        //_delay_ms (90);
        modSysClock_wait (50, SYSCLOCK_GET_TIME_MS_1);
        TIM2->CR1 &= ~TIM_CR1_CEN; //drvBUZZER_off ();
        //_delay_ms (90);
        modSysClock_wait (50, SYSCLOCK_GET_TIME_MS_1);
        numbers--;
    }
}


rtc_s mRTC;

void modRTC_cnt_get (rtc_s *rtc)
{
    rtc->year = bcd2dec ((RTC->DR & (RTC_DR_YT | RTC_DR_YU)) >> 16) + 2000;
    rtc->month = bcd2dec ((RTC->DR & (RTC_DR_MT | RTC_DR_MU)) >> 8);
    rtc->day = bcd2dec (RTC->DR & (RTC_DR_DT | RTC_DR_DU));
    //RTC->TSDR & (RTC_DR_WDU)) >> 13
    rtc->hour = bcd2dec ((RTC->TR & (RTC_TR_HT | RTC_TR_HU)) >> 16);
    rtc->minute = bcd2dec ((RTC->TR & (RTC_TR_MNT | RTC_TR_MNU)) >> 8);
    rtc->second = bcd2dec (RTC->TR & (RTC_TR_ST | RTC_TR_SU));
}


void modRTC_cnt_set (rtc_s *rtc)
{
    RCC->APB1ENR |= RCC_APB1ENR_PWREN;
    
    //Enable write access to Backup domain
    PWR->CR |= PWR_CR_DBP;
    while ((PWR->CR & PWR_CR_DBP) == 0) {};
    
    // reset Backup 
    RCC->BDCR |= RCC_BDCR_BDRST;
    RCC->BDCR &= ~RCC_BDCR_BDRST;

    // On LSE
    RCC->BDCR |= RCC_BDCR_LSEON;
    while ((RCC->BDCR & RCC_BDCR_LSERDY) != RCC_BDCR_LSERDY) {}; //waiting on

    
     //������� LSE �������� (����� 32768) � ������ ������������
    RCC->BDCR |= RCC_BDCR_RTCEN; // RTC on
    RCC->BDCR &= ~RCC_BDCR_RTCSEL;
    RCC->BDCR |= RCC_BDCR_RTCSEL_0; //LSE
        
    
    //enable access to RTC
    RTC->WPR = 0xCA;
    RTC->WPR = 0x53; 
    
    //while ((RTC->ISR & RTC_ISR_RSF) == 0) {};
    
    // Set the Initialization mode
    RTC->ISR |= RTC_ISR_INIT;
    // Wait till RTC is in INIT state and if Time out is reached exit
    while ((RTC->ISR & RTC_ISR_INITF) == 0) {};
    
    // Clear RTC_CR FMT, OSEL and POL Bits
    RTC->CR &= ((U32)~(RTC_CR_FMT | RTC_CR_OSEL | RTC_CR_POL));
    // Set RTC_CR register
    RTC->CR |= 0;
    
    // Configure the RTC PRER
    RTC->PRER = (U32)0x007F00FF; // div to 32768
    
    U32 tmpu32;
    tmpu32 = 0;
    tmpu32 = (U32)dec2bcd (rtc->year - 2000) << 16;
    tmpu32 |= (U32)dec2bcd (rtc->month) << 8;
    tmpu32 |= (U32)dec2bcd (rtc->day);
    RTC->DR = tmpu32;
    tmpu32 = 0;
    tmpu32 = (U32)dec2bcd (rtc->hour) << 16;
    tmpu32 |= (U32)dec2bcd (rtc->minute) << 8;
    tmpu32 |= (U32)dec2bcd (rtc->second);
    RTC->TR = tmpu32;
    
        //RTC->CALR |= 0; //calibration RTC
                
    // off test pin? TODO
    RTC->TAFCR &= (U32)~RTC_TAFCR_PC13VALUE;
    
    // Exit Initialization mode
    RTC->ISR &= (U32)~RTC_ISR_INIT; 
    
    //disable access to RTC
    RTC->WPR = 0xFF;
}


BOOL RTC_need_set = FALSE;

U8 modRTC_init (void)
{
    //���� ���� ��������� - ���������������� ��
    if ((((RCC->BDCR & RCC_BDCR_RTCEN) != RCC_BDCR_RTCEN) || (TRUE == RTC_need_set))
        || (RTC_need_set == TRUE))
    {
        mRTC.year   = 2018;
        mRTC.month  = 4;
        mRTC.day    = 16;
        mRTC.hour   = 8;
        mRTC.minute = 59;
        mRTC.second = 0;
        //mRTC.millisecond = 0;
        
        modRTC_cnt_set (&mRTC);
        RTC_need_set = FALSE;
        return 2;
    }
    else
    {
        modRTC_cnt_get (&mRTC);
        RTC_need_set = FALSE;
        return 1;
    }
}



U8 GPIO_OLED_DC_val;

void halSPI2_GPIO_SPI_init (void)
{
    // SPI2 - PB13 - SCK,PB15 - MOSI
        
    // Enable clock
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    
    //OLED pins
    GPIOB->MODER &= ~(GPIO_MODER_MODER12 | GPIO_MODER_MODER14);
    GPIOB->MODER |= (GPIO_MODER_MODER12_0 | GPIO_MODER_MODER14_0);
    GPIOB->OTYPER &= ~(GPIO_OTYPER_OT_12 | GPIO_OTYPER_OT_14);
    GPIOB->OSPEEDR |= (GPIO_OSPEEDR_OSPEEDR12 | GPIO_OSPEEDR_OSPEEDR14);
    GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR12 | GPIO_PUPDR_PUPDR14);
    GPIO_OLED_CS_H;
    GPIO_OLED_DC_H;
    
#if SPI2_SOFTAWARE
    GPIOB->MODER &= ~(GPIO_MODER_MODER13 | GPIO_MODER_MODER15);
    GPIOB->MODER |= (GPIO_MODER_MODER13_0 | GPIO_MODER_MODER15_0);
    GPIOB->OTYPER &= ~(GPIO_OTYPER_OT_13 | GPIO_OTYPER_OT_15);
    GPIOB->OSPEEDR |= (GPIO_OSPEEDR_OSPEEDR13 | GPIO_OSPEEDR_OSPEEDR15);
    GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR13 | GPIO_PUPDR_PUPDR15);
    
    
#else
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    GPIOB->MODER &= ~(GPIO_MODER_MODER15 | GPIO_MODER_MODER13);
    GPIOB->MODER |= (GPIO_MODER_MODER15_1 | GPIO_MODER_MODER13_1);
    GPIOB->OTYPER &= ~(GPIO_OTYPER_OT_15 | GPIO_OTYPER_OT_13);
    GPIOB->OSPEEDR |= (GPIO_OSPEEDR_OSPEEDR15 | GPIO_OSPEEDR_OSPEEDR13);
    GPIOB->PUPDR &= ~(GPIO_PUPDR_PUPDR15 | GPIO_PUPDR_PUPDR13);
    GPIOB->AFR[1] &= ~(GPIO_AFRH_AFRH7 | GPIO_AFRH_AFRH5); //AF0
    
    RCC->APB1ENR |= (RCC_APB1ENR_SPI2EN);
    
    SPI2->CR1   &= ~SPI_CR1_SPE;		 //��������� SPI ����� ����������� ��������
    
    // Set SPI1
	SPI2->CR1 =
		SPI_CR1_CPHA * 0 |		// Clock Phase
		SPI_CR1_CPOL * 0 |		// Clock Polarity
		SPI_CR1_MSTR * 1 |		// Master Selection
		SPI_CR1_BR_0 * 1 |		// Baud Rate Control - 72 / x = x MHz
		SPI_CR1_BR_1 * 0 |		//
		SPI_CR1_BR_2 * 0 |		//
		SPI_CR1_SPE * 0 |		// SPI Enable
		SPI_CR1_LSBFIRST * 0 |		// Frame Format
		SPI_CR1_SSI * 1 |		// Internal slave select
		SPI_CR1_SSM * 1 |		// Software slave management
		SPI_CR1_RXONLY * 0 |		// Receive only
		SPI_CR1_CRCNEXT * 0 |		// Transmit CRC next
		SPI_CR1_CRCEN * 0 |		// Hardware CRC calculation enable
		SPI_CR1_BIDIOE * 0 |		// Output enable in bidirectional mode
		SPI_CR1_BIDIMODE * 0;		// Bidirectional data mode enable

	SPI2->CR2 =
        SPI_CR2_DS_0 * 0 | //9 bit
        SPI_CR2_DS_1 * 0 |
        SPI_CR2_DS_2 * 0 |
        SPI_CR2_DS_3 * 1 |
		SPI_CR2_RXDMAEN * 0 |		// Rx Buffer DMA Enable
		SPI_CR2_TXDMAEN * 1 |		// Tx Buffer DMA Enable
		SPI_CR2_SSOE * 0 |		// SS Output Enable
		//SPI_CR2_FRF * 0 |	// Protocol format - 0: SPI Motorola mode, 1: SPI TI mode
		SPI_CR2_ERRIE * 0 |		// Error Interrupt Enable
		SPI_CR2_RXNEIE * 0 |		// RX buffer Not Empty Interrupt Enable
		SPI_CR2_TXEIE * 0;		// Tx buffer Empty Interrupt Enable

	SPI2->CR1 |= SPI_CR1_SPE;	// ���������, ������
    
    /*
    //DMA_Init
	DMA1_Stream4->CR = 0; //off stream
	DMA1_Stream4->PAR = (uint32_t)&(SPI2->DR);
	DMA1_Stream4->M0AR = 0;//(uint32_t)&-; //TODO
	DMA1_Stream4->NDTR = 0;
	DMA1_Stream4->CR =
        DMA_SxCR_CHSEL_0 * 0 | 
        DMA_SxCR_CHSEL_1 * 0 |
        DMA_SxCR_CHSEL_2 * 0 |
            
        DMA_SxCR_MBURST_0 * 0 |
        DMA_SxCR_MBURST_1 * 0 |
            
        DMA_SxCR_PBURST_0 * 0 |
        DMA_SxCR_PBURST_1 * 0 |
            
        DMA_SxCR_CT * 0 | //Current target - memory0
        DMA_SxCR_DBM * 0 | //Double buffer mode

		DMA_SxCR_PL_0 * 0 | // PL[1:0]: Priority level | 11: Very high.  PL[1:0]: Priority level
		DMA_SxCR_PL_1 * 1 |// |  
            
        DMA_SxCR_PINCOS * 0 | //Peripheral increment offset size
            
		DMA_SxCR_MSIZE_0 * 0 | // MSIZE[1:0]: Memory data size | 01: 16-bit
		DMA_SxCR_MSIZE_1 * 0 | //TODO ������ ������ 8-���???
            
		DMA_SxCR_PSIZE_0 * 0 |// PSIZE[1:0]: Peripheral data size | 01: 16-bit
		DMA_SxCR_PSIZE_1 * 0 |
            
		DMA_SxCR_MINC * 1 | // MINC: Memory increment mode | 1: Memory address pointer is incremented after
        DMA_SxCR_PINC * 0 | //// PINC: Peripheral increment mode | 0: Peripheral address pointer is fixed
		DMA_SxCR_CIRC * 0 | // CIRC: Circular mode disabled
		DMA_SxCR_DIR_0 * 1 |  // DIR: Data transfer direction - Memory-to-peripheral
        DMA_SxCR_DIR_1 * 0 |
        DMA_SxCR_PFCTRL * 0 |
		DMA_SxCR_TCIE * 1 | //Transfer interrupt
		DMA_SxCR_HTIE * 0 | //Half transfer interrupt
		DMA_SxCR_TEIE * 0 | //Error interrupt
        DMA_SxCR_DMEIE * 0 | //Direct mode error interrupt enable
		DMA_SxCR_EN * 0; // EN: Stream enable | 1: Stream enabled
    */
	
#endif
}


U8 halSPI2_xspi (U8 data)
{
    U16 tmp = 0;
#if SPI2_SOFTAWARE
    U8 i;
    if (GPIO_OLED_DC_val)
    {
        GPIOB->BSRR = GPIO_BSRR_BS_15;
    } else {
        GPIOB->BSRR = GPIO_BSRR_BR_15;
    }
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    GPIOB->BSRR = GPIO_BSRR_BS_13;
    __NOP();
    __NOP();
    __NOP();
    __NOP();
    GPIOB->BSRR = GPIO_BSRR_BR_13;
    __NOP();
    __NOP();
    __NOP();
    __NOP();
        
    for (i = 0; i < 8; i++)
    {
        if (data & 0x80)
        {
            GPIOB->BSRR = GPIO_BSRR_BS_15;
        } else {
            GPIOB->BSRR = GPIO_BSRR_BR_15;
        }
        data = data << 1;
        __NOP();
        __NOP();
        __NOP();
        __NOP();
        GPIOB->BSRR = GPIO_BSRR_BS_13;
        __NOP();
        __NOP();
        __NOP();
        __NOP();
        GPIOB->BSRR = GPIO_BSRR_BR_13;
	    __NOP();
        __NOP();
        __NOP();
        __NOP();
    }
#else
    tmp = data;
    if (GPIO_OLED_DC_val)
    {
        tmp = tmp | 0x0100;
    }
    while ((SPI2->SR & SPI_SR_TXE) == 0) {};
    SPI2->DR = tmp;
    //while ((SPI2->SR & SPI_SR_RXNE) != 0) {}; 
    //while ((SPI2->SR & SPI_SR_BSY) != 0) {};
    //tmp = SPI2->DR;
#endif
    return tmp;
}


U8 halSPI2_mxspi (U8 *data, U32 num)
{
    U16 tmp = 0;
#if SPI2_SOFTAWARE
    
#else
    //DMA1_Stream4->NDTR = num; //reinit count
	//DMA1_Stream4->CR |= (U16)(DMA_SxCR_EN); // start!
    //tmp = data;
    if (GPIO_OLED_DC_val)
    {
    //    tmp = tmp | 0x0100;
    }
    while ((SPI2->SR & SPI_SR_TXE) == 0) {};
    //SPI2->DR = tmp;
    //while ((SPI2->SR & SPI_SR_RXNE) != 0) {}; 
    //while ((SPI2->SR & SPI_SR_BSY) != 0) {};
    //tmp = SPI2->DR;
#endif
    return tmp;
}


void halADC_init (void)
{
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
    GPIOA->MODER |= GPIO_MODER_MODER0;
    GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR0;
    
    RCC->APB2ENR |= RCC_APB2ENR_ADC1EN;
    // TODO add calibration
	ADC1->CR |= ADC_CR_ADEN;              // 
    ADC1->CHSELR = 0; 
    ADC1->CHSELR |= ADC_CHSELR_CHSEL0; 
    ADC1->SMPR &= ~(ADC_SMPR_SMP);
    //ADC1->SMPR |= ADC_SMPR_SMP;
    ADC1->CFGR1 |= ADC_CFGR1_DISCEN; 
    //ADC1->CR |= ADC_CR_ADSTART;
}


void halADC_deinit (void)
{
    RCC->APB2ENR &= ~RCC_APB2ENR_ADC1EN;
}


U16 halADC_get_BATT_mV (void)
{
    U32 i;
    
    U32 adc_val = 0;
    for (i = 0; i < 16; i++)
    {
        ADC1->CR |= ADC_CR_ADSTART; // start conversion
        while ((ADC1->ISR & ADC_ISR_EOS) == 0) {}; // waiting end of conversion
        adc_val += (U32)ADC1->DR;
    }
    adc_val = adc_val * 3300 * 2;
    adc_val = adc_val / (4096 - 1);
    adc_val = adc_val / 16;
    
    return (U16)adc_val;
}


void hal_vibro_ON (void)
{
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
    GPIOA->MODER &= ~GPIO_MODER_MODER7;
    GPIOA->MODER |= GPIO_MODER_MODER7_0;
    GPIOA->OTYPER &= ~GPIO_OTYPER_OT_7;
    GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR7;
    GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR7;
    GPIOA->BSRR = GPIO_BSRR_BS_7;
}


void hal_vibro_OFF (void)
{
    RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
    GPIOA->MODER &= ~GPIO_MODER_MODER7;
    GPIOA->MODER |= GPIO_MODER_MODER7_0;
    GPIOA->OTYPER &= ~GPIO_OTYPER_OT_7;
    GPIOA->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR7;
    GPIOA->PUPDR &= ~GPIO_PUPDR_PUPDR7;
    GPIOA->BSRR = GPIO_BSRR_BR_7;
}


void hal_ESP8266_ON (void)
{
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    GPIOB->MODER &= ~GPIO_MODER_MODER2;
    GPIOB->MODER |= GPIO_MODER_MODER2_0;
    GPIOB->OTYPER &= ~GPIO_OTYPER_OT_2;
    GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR2;
    GPIOB->PUPDR &= ~GPIO_PUPDR_PUPDR2;
    GPIOB->BSRR = GPIO_BSRR_BS_2;
}


void hal_ESP8266_OFF (void)
{
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    GPIOB->MODER &= ~GPIO_MODER_MODER2;
    GPIOB->MODER |= GPIO_MODER_MODER2_0;
    GPIOB->OTYPER &= ~GPIO_OTYPER_OT_2;
    GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR2;
    GPIOB->PUPDR &= ~GPIO_PUPDR_PUPDR2;
    GPIOB->BSRR = GPIO_BSRR_BR_2;
}


void hal_SIM800_ONOFF (void)
{
    RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
    GPIOB->MODER &= ~GPIO_MODER_MODER8;
    GPIOB->MODER |= GPIO_MODER_MODER8_0;
    GPIOB->OTYPER |= GPIO_OTYPER_OT_8;
    GPIOB->OSPEEDR |= GPIO_OSPEEDR_OSPEEDR8;
    GPIOB->PUPDR &= ~GPIO_PUPDR_PUPDR8;
    GPIOB->BSRR = GPIO_BSRR_BR_8;
    _delay_ms (1100);
    GPIOB->BSRR = GPIO_BSRR_BS_8;
    _delay_ms (10);
}


//
void RTC_year_set (S8 t)
{
    if (+1 == t)
        if (++mRTC.year > 40000) mRTC.year = 1; //it's WH40000!!1! 
    if (-1 == t)
        if (--mRTC.year < 1) mRTC.year = 40000;
}


void RTC_year_get (char *str)
{
    xsprintf (str, "%u", mRTC.year);
}


void RTC_month_set (S8 t)
{
    if (+1 == t)
        if (++mRTC.month > 12) mRTC.month = 1;
    if (-1 == t)
        if (--mRTC.month < 1) mRTC.month = 12;
}


void RTC_month_get (char *str)
{
    xsprintf (str, "%u", mRTC.month);
}


void RTC_day_set (S8 t)
{
    if (+1 == t)
        if (++mRTC.day > 31) mRTC.day = 1;
    if (-1 == t)
        if (--mRTC.day < 1) mRTC.day = 31;
}


void RTC_day_get (char *str)
{
    xsprintf (str, "%u", mRTC.day);
}


void RTC_hour_set (S8 t)
{
    if (+1 == t)
        if (++mRTC.hour > 23) mRTC.hour = 0;
    if (-1 == t)
        if (--mRTC.hour < 1) mRTC.hour = 23;
}


void RTC_hour_get (char *str)
{
    xsprintf (str, "%u", mRTC.hour);
}


void RTC_minute_set (S8 t)
{
    if (+1 == t)
        if (++mRTC.minute > 59) mRTC.minute = 0;
    if (-1 == t)
        if (--mRTC.minute < 1) mRTC.minute = 59;
}


void RTC_minute_get (char *str)
{
    xsprintf (str, "%u", mRTC.minute);
}


void RTC_second_set (S8 t)
{
    if (+1 == t) { //@todo ��������� ������������
        if (mRTC.second >= 59) mRTC.second = 0; else mRTC.second++;
    }
    if (-1 == t) {
        if (mRTC.second == 0) mRTC.second = 59; else mRTC.second--;
    }
}


void RTC_second_get (char *str)
{
    xsprintf (str, "%u", mRTC.second);
}


void RTC_redact (S8 t)
{
    modRTC_cnt_get (&mRTC);
}


void RTC_save (S8 t)
{
    RTC_need_set = TRUE;
    modRTC_cnt_set (&mRTC);
}


// ������� 2
void contrast_set (S8 t)
{
    if (+1 == t)
        sEEPROM_config.OLED_contrast++; // ��� �������� ������, �� 0 �� 255 ������������
    if (-1 == t)
        sEEPROM_config.OLED_contrast--;
    hal_paint_set_contrast (sEEPROM_config.OLED_contrast);
}


void contrast_get (char *str)
{
    xsprintf (str, "%u", sEEPROM_config.OLED_contrast);
}


void contrast_save (S8 t)
{
   drvStorage_save ();
}


char *config_get_SSID(void);

char *config_get_PASS(void);

void config_set_modbus_timeout(U32 delay);
U32 config_get_modbus_timeout(void);

void config_set_auto_send_delay(U32 delay);
U32 config_get_auto_send_delay(void);




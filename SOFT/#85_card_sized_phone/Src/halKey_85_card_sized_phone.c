/*
 * @file    
 * @author  Ht3h5793
 * @date    10.03.2014
 * @version V9.0.0
 * @brief  
*/

#include "halKey.h"
#include "modKey_local.h"
#include "board.h"
#include "modSysClock.h"
#include "drvPCA8885.h"

/** �������� ��������� ������ */
#define MODKEY_PIN_STATE_ACTIVE         1
#define MODKEY_PIN_STATE_NOACTIVE       0

// ��� ���� �������� ������ �������������� ���������
U8 halKeyCnt [NUMBER_KEYS];
SYSTIME key_delay = 0;


void halKey_init (void)
{
    U8 i;
    
    // PCAS8885 test
    if (FUNCTION_RETURN_OK == drvPCA8885_init ())
	{
		//num_bytes = xsprintf (_str, "drvPCA8885_init OK\r\n");
	}
	else
	{
        while (1) {};
		//num_bytes = xsprintf (_str, "drvPCA8885_init ERROR!\r\n");
	}
    //debug_out (_str, num_bytes);
    
    __DI();
    // ���������� ������� ������ ���������� �� ���������
    for (i = 0; i < NUMBER_KEYS; i++)
    {
        halKeyCnt[i] = HALKEY_COUNTER_V_MIN;
    }
    __EI();
}

U8 key;
void halKey_run (void)
{
    U8 i = 0;
    
    
    if (FUNCTION_RETURN_OK == modSysClock_timeout (&key_delay, 1, SYSCLOCK_GET_TIME_MS_1))
    {
        
        
        if (FUNCTION_RETURN_OK == drvPCA8885_get (&key))
        {
            //num_bytes = xsprintf (_str, "Key: 0x%02X\r\n", key);
            //debug_out (_str, num_bytes);
        }
        
        for (i = 0; i < NUMBER_KEYS; i++)  
        {
            U8 PinState = MODKEY_PIN_STATE_NOACTIVE;
            switch (i)
            {
            case KEY_0:
                if (HKEY_0 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
 
            case KEY_1:
                if (HKEY_1 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_2:
                if (HKEY_2 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_3:
                if (HKEY_3 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_4:
                if (HKEY_4 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_5:
                if (HKEY_5 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_6:
                if (HKEY_6 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_7:
                if (HKEY_7 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_8:
                if (HKEY_8 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_9:
                if (HKEY_9 == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_DOT:
                if (HKEY_DOT == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_EQUAL:
                if (HKEY_EQUAL == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_UP:
                if (HKEY_UP == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                
            case KEY_DOWN:
                if (HKEY_DOWN == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_LEFT:
                if (HKEY_LEFT == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            case KEY_RIGHT:
                if (HKEY_RIGHT == key)
                {
                    PinState =  MODKEY_PIN_STATE_ACTIVE;
                }
                break;
                
            default: break;
            }
            // ���������� �������� ���������
            if (MODKEY_PIN_STATE_ACTIVE == PinState)
            {
                halKeyCnt[i]++;
                if (halKeyCnt[i] > HALKEY_COUNTER_V_MAX)
                {
                    halKeyCnt[i] = HALKEY_COUNTER_V_MAX;
                } 
            }
            else
            {
                halKeyCnt[i]--;
                if (halKeyCnt[i] < HALKEY_COUNTER_V_MIN)
                {
                    halKeyCnt[i] = HALKEY_COUNTER_V_MIN;
                } 
            }
        }
    }
}


U8 halKey_getCount (U8 num)
{
    return halKeyCnt[num];
}

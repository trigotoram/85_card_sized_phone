#include "_fifo.h"
#include "board.h"


#define FIFO_POS     (s->in - s->out)

void FIFO_flush ( FIFO_t *s)
{
    s->in = s->out;
}


BOOL FIFO_init (FIFO_t *s, FIFO_type *buf, U32 FIFO_size)
{
    if ((NULL == buf) || 
        (FIFO_size < 2) || // FIFO size is too small.  It must be larger than 1
        ((FIFO_size & (FIFO_size - 1)) != 0)) //must be a power of 2
    {
        return FALSE;
    }
    else
    {
        __disable_irq();
        s->buf = buf;
        s->size = FIFO_size;
        __enable_irq();
        FIFO_flush(s);
        return TRUE;
    }
}


U32 FIFO_available (FIFO_t *s) //TODO data size
{
    S32 i;

    __disable_irq();
    i = FIFO_POS;
    __enable_irq();
    if (i < 0)
      i = -i;
    return (U32)i;
}


BOOL FIFO_get (FIFO_t *s, FIFO_type *c) //TODO data size
{
    if (FIFO_available(s) > 0)
    {
        __disable_irq();
        *c = (s->buf [(s->out) & (s->size - 1)]);
        s->out++;
        __enable_irq();
        return  TRUE;
    }
    return FALSE;
}


BOOL FIFO_put (FIFO_t *s, FIFO_type c)
{
    __disable_irq ();
    if (FIFO_POS < s->size) // If the buffer is full, return an error value
    {
        s->buf [s->in & (s->size - 1)] = c; 
        s->in++;
        __enable_irq ();
        return TRUE;
    }
    __enable_irq ();
    return FALSE;
}


BOOL FIFO_gets (FIFO_t *s, FIFO_type *c, U32 data_size) //TODO data size
{
    U32 i;

    if (NULL == c)
        return FALSE;
    if (data_size > s->size)
        return FALSE;
    if (FIFO_available (s) >= data_size)
    {
        __disable_irq ();
        for (i = 0; i < data_size; i++)
        {
            c[i] = (s->buf [(s->out) & (s->size - 1)]);
            s->out++;
        }
        __enable_irq ();
        return TRUE;
    }
    return FALSE;
}


BOOL FIFO_puts (FIFO_t *s, FIFO_type *c, U32 data_size) 
{
    U32 i;
    
    if (NULL == c)
        return FALSE;
    if (data_size > s->size)
        return FALSE;
    if (s->size - FIFO_available (s) >= data_size) // If the buffer is full, return an error value
    {
        __disable_irq ();
        for (i = 0; i < data_size; i++)
        {
            s->buf [s->in & (s->size - 1)] = c[i]; 
            s->in++;
        }
        __enable_irq ();
        return TRUE;
    }
    return FALSE;
}

// http://git.rockbox.org/?p=rockbox.git;a=blob;f=apps/plugins/mosaique.c;h=68938a1f136ebd6f03ef0d7dfe49e13acfd8678c;hb=HEAD

#include "board.h"
#include "halPaint.h"
#include "modPaint.h"
#include "modRandom.h"


void screensaver_life_init (void);
void screensaver_life_draw (void);
extern U8 screen_buf [SCREEN_VIRTUAL_W * SCREEN_VIRTUAL_H / 8];

void screen_saver_life_init (void)
{
    COORD x, y;

    for (y = 0; y < SCREEN_H; y++)
    {
        for (x = 0; x < SCREEN_W; x++)
        {
            if (_rand1() == 0)
            {
                paint_pixelColor (x, y, COLOR_WHITE);
            }
            else
            {
                paint_pixelColor (x, y, COLOR_BLACK);
            }
        }
    }
    paint_setColor (COLOR_WHITE);
    paint_setBackgroundColor (COLOR_BLACK);
    hal_paint_update ();
}


void screensaver_life_draw (void)
{
    COORD y1, x1;
    COORD x, y, n;
	U8 screen_buf_ [SCREEN_VIRTUAL_W * SCREEN_VIRTUAL_H / 8];
 
	for (y = 0; y < SCREEN_H; y++)
    {
        for (x = 0; x < SCREEN_W; x++)
        {
            n = 0;
            for (y1 = y - 1; y1 <= y + 1; y1++)
            {
                for (x1 = x - 1; x1 <= x + 1; x1++)
                {
                    if (hal_paint_getPixel ((x1 + SCREEN_W) % SCREEN_W, (y1 + SCREEN_H) % SCREEN_H) != 0)
                    {
                        n++;
                    }
                }
            }
            if (hal_paint_getPixel (x, y) != 0)
            {
                n--;
            }
            if ((n == 3 || (n == 2 && hal_paint_getPixel (x, y))) != 0)
            {
                screen_buf_ [x + (y >> 3) * SCREEN_W] |=  (1 << (y & 7));
            }
            else
            {
                screen_buf_ [x + (y >> 3) * SCREEN_W] &= ~(1 << (y & 7));
            }
        }
    }
	memcpy ((void *)&screen_buf [0], (void *)&screen_buf_[0], SCREEN_W * SCREEN_H / 8);
    hal_paint_update ();
}
 

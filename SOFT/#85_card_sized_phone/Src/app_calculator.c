// http://git.rockbox.org/?p=rockbox.git;a=blob;f=apps/plugins/mosaique.c;h=68938a1f136ebd6f03ef0d7dfe49e13acfd8678c;hb=HEAD

#include "board.h"
#include "halPaint.h"
#include "modPaint.h"
//#include "modSysClock.h"
#include "modRandom.h"


void add_to_str(U8 *source,U8 *dest,U8 lng,U8 pos)
{
    string str ((const char*)dest);
    str.insert(pos,(const char*)source,lng);
    str.copy((char*)dest,50,0);
}  


void cut_from_str(U8 *source,U8 pos,U8 lng)
{
    U8 start;
    U8 i;
    string str ((const char*)source);
    str.erase(pos,lng);
    start=str.size();
    for (i=start;i<start+lng;i++){source[i]=0;}//������� ��������� ������� �� ������(����� ��������� ��� �����������)
    str.copy((char*)source,50,0);
}  

void cut_chr_from_str(U8 *source,U8 pos)
{
    U8 i;
    string str ((const char*)source);
    str.erase(pos,1);
    i=str.size();
    source[i]=0;//������� ��������� ������ �� ������(����� ��������� ��� �����������)
    str.copy((char*)source,50,0);
} 


void replace_by_char(U8 *source,U8 pos,U8 lng,U8 chr)
{
    //������� �� ������ ����� �������� � �������� �� ��������
    cut_from_str(source,pos,lng);
    add_to_str(&chr,source,1,pos);
} 













#include "stdint.h"

void button_handler(U8 butt_code);

void add_plus(void);
void add_minus(void);
void add_multiply(void);
void add_div(void);

void add_dot(void);
void add_E(void);
void add_pow(void);
void add_dbl(void);
void add_sqrt(void);
void add_ln(void);
void add_exp(void);
void add_min(void);
void add_pi(void);

void add_brackets(void);

void add_n0(void);
void add_n1(void);
void add_n2(void);
void add_n3(void);
void add_n4(void);
void add_n5(void);
void add_n6(void);
void add_n7(void);
void add_n8(void);
void add_n9(void);

void add_sin(void);
void add_cos(void);
void add_tg(void);

void move_cursor_left(void);
void move_cursor_right(void);

void move_dialog_cursor_left(void);
void move_dialog_cursor_right(void);
void clear_input_buffer(void);
void delete_symbol(void);

void fill_structures(void);

#define plus_butt_code    62
#define plus_butt_lng    1
#define plus_butt_txt    "+"
#define plus_butt_move   1

#define minus_butt_code    52
#define minus_butt_lng    1
#define minus_butt_txt    "-"

#define multiply_butt_code    42
#define multiply_butt_lng    1
#define multiply_butt_txt    "*"

#define div_butt_code    32
#define div_butt_lng    1
#define div_butt_txt    "/"


#define sin_butt_code    13
#define sin_butt_lng    5
#define sin_butt_txt    "sin()"

#define cos_butt_code    12
#define cos_butt_lng    5
#define cos_butt_txt    "cos()"

#define tg_butt_code   11 
#define tg_butt_lng    4
#define tg_butt_txt    "tg()"


#define n0_butt_code   66
#define n0_butt_lng    1
#define n0_butt_txt    "0"


#define n1_butt_code   56
#define n1_butt_lng    1
#define n1_butt_txt    "1"

#define n2_butt_code   55
#define n2_butt_lng    1
#define n2_butt_txt    "2"

#define n3_butt_code   53
#define n3_butt_lng    1
#define n3_butt_txt    "3"

#define n4_butt_code   46
#define n4_butt_lng    1
#define n4_butt_txt    "4"

#define n5_butt_code   45
#define n5_butt_lng    1
#define n5_butt_txt    "5"

#define n6_butt_code   43
#define n6_butt_lng    1
#define n6_butt_txt    "6"

#define n7_butt_code   36
#define n7_butt_lng    1
#define n7_butt_txt    "7"

#define n8_butt_code   35
#define n8_butt_lng    1
#define n8_butt_txt    "8"

#define n9_butt_code   33
#define n9_butt_lng    1
#define n9_butt_txt    "9"

#define dot_butt_code   65
#define dot_butt_lng    1
#define dot_butt_txt    "."

#define E_butt_code   16
#define E_butt_lng    1
#define E_butt_txt    "E"

#define pow_butt_code   25
#define pow_butt_lng    1
#define pow_butt_txt    "^"

#define dbl_butt_code   26//^2
#define dbl_butt_lng    1
#define dbl_butt_txt    ""

#define min_butt_code   63//  /-/
#define min_butt_lng    1
#define min_butt_txt    ""


#define brackets_butt_code   23
#define brackets_butt_lng    2
#define brackets_butt_txt    "()"

#define sqrt_butt_code   26
#define sqrt_butt_lng    3

#define exp_butt_code   15
#define exp_butt_lng    5
#define exp_butt_txt    "exp()"

#define ln_butt_code   15
#define ln_butt_lng    4
#define ln_butt_txt    "ln()"

#define pi_butt_code   3
#define pi_butt_lng    1







#include "stdio.h"
#include "intrinsics.h"
#include "stdint.h"


extern U8 input_buffer[50];
extern U8 input_buffer_cursor;
extern U8 input_buffer_length;
extern U8 current_mode;//0-����1
extern U8 dialog_cursor_pos;
extern U8 dialog_flag;




void button_handler(U8 butt_code)
{
  if (current_mode==0)
  {
    switch (butt_code)
    {
      case 22: move_cursor_left();break;
      case 21: move_cursor_right();break;
      case 4:  delete_symbol();break;
      
      
      case plus_butt_code: add_plus(); break;//plus
      case minus_butt_code: add_minus();break;//minus
      case multiply_butt_code: add_multiply();break;//multiply
      case div_butt_code: add_div();break;//multiply
      
      case n0_butt_code: add_n0();break;//0
      case n1_butt_code: add_n1();break;
      case n2_butt_code: add_n2();break;
      case n3_butt_code: add_n3();break;
      case n4_butt_code: add_n4();break;
      case n5_butt_code: add_n5();break;
      case n6_butt_code: add_n6();break;
      case n7_butt_code: add_n7();break;
      case n8_butt_code: add_n8();break;
      case n9_butt_code: add_n9();break;
      
      case sin_butt_code: add_sin();break;
      case cos_butt_code: add_cos();break;
      case tg_butt_code: add_tg();break;
      case ln_butt_code: add_ln();break;
      
      case dot_butt_code: add_dot();break;
      case E_butt_code:   add_E();break;//1^10
      case pow_butt_code: add_pow();break;//^
      case dbl_butt_code: add_dbl();break;//^2
      case pi_butt_code:  add_pi();break;//PI
      case brackets_butt_code: add_brackets();break;
      case min_butt_code: add_min();break;
      
      case 5: clear_input_buffer();break;
      case 1: current_mode=2;return;break;
      case 31: current_mode=3;return;break;//save
      case 41: current_mode=4;return;break;//load
      
      case 61: solve(input_buffer,input_buffer_length);break;
    }
    
  }//end if mode==0
  
  if (current_mode==2)
  {
    current_mode=1;
    switch (butt_code)
    {
      case sqrt_butt_code: add_sqrt();break;//sqrt
      case exp_butt_code: add_exp();break;//sqrt
      case 61: solve(input_buffer,input_buffer_length);break;
    }
    
  }//end of mode==2
  
  if (current_mode==3)
  {
    switch (butt_code)
    {
      case 22: move_dialog_cursor_left();break;
      case 21: move_dialog_cursor_right();break;
      case n0_butt_code: dialog_cursor_pos=0;break;//0
      case n1_butt_code: dialog_cursor_pos=1;break;
      case n2_butt_code: dialog_cursor_pos=2;break;
      case n3_butt_code: dialog_cursor_pos=3;break;
      case 61: dialog_flag=1;break;
      case 5: current_mode=0;break;//exit dialog
    }
    
  }//end of mode==3
  
  if (current_mode==4)
  {
    switch (butt_code)
    {
      case 22: move_dialog_cursor_left();break;
      case 21: move_dialog_cursor_right();break;
      case n0_butt_code: dialog_cursor_pos=0;break;//0
      case n1_butt_code: dialog_cursor_pos=1;break;
      case n2_butt_code: dialog_cursor_pos=2;break;
      case n3_butt_code: dialog_cursor_pos=3;break;
      case 61: dialog_flag=1;break;
      case 5: current_mode=0;break;//exit dialog
    }
    
  }//end of mode==4
  
  
  
}



void add_plus(void)
{
  add_to_str((U8*)plus_butt_txt,input_buffer,plus_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+plus_butt_lng;
  input_buffer_length++;
}

void add_minus(void)
{
  add_to_str((U8*)minus_butt_txt,input_buffer,minus_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+minus_butt_lng;
  input_buffer_length++;
}

void add_multiply(void)
{
  add_to_str((U8*)multiply_butt_txt,input_buffer,multiply_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+multiply_butt_lng;
  input_buffer_length++;
}

void add_div(void)
{
  add_to_str((U8*)div_butt_txt,input_buffer,div_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+div_butt_lng;
  input_buffer_length++;
}


void add_sin(void)
{
  add_to_str((U8*)sin_butt_txt,input_buffer,sin_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+sin_butt_lng-1;
  input_buffer_length=input_buffer_length+sin_butt_lng;
}

void add_cos(void)
{
  add_to_str((U8*)cos_butt_txt,input_buffer,cos_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+cos_butt_lng-1;
  input_buffer_length=input_buffer_length+cos_butt_lng;
}

void add_tg(void)
{
  add_to_str((U8*)tg_butt_txt,input_buffer,tg_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+tg_butt_lng-1;
  input_buffer_length=input_buffer_length+tg_butt_lng;
}

void add_n0(void)
{
  add_to_str((U8*)n0_butt_txt,input_buffer,n0_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n0_butt_lng;
  input_buffer_length++;
}
void add_n1(void)
{
  add_to_str((U8*)n1_butt_txt,input_buffer,n1_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n1_butt_lng;
  input_buffer_length++;
}
void add_n2(void)
{
  add_to_str((U8*)n2_butt_txt,input_buffer,n2_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n2_butt_lng;
  input_buffer_length++;
}
void add_n3(void)
{
  add_to_str((U8*)n3_butt_txt,input_buffer,n3_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n3_butt_lng;
  input_buffer_length++;
}
void add_n4(void)
{
  add_to_str((U8*)n4_butt_txt,input_buffer,n4_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n4_butt_lng;
  input_buffer_length++;
}
void add_n5(void)
{
  add_to_str((U8*)n5_butt_txt,input_buffer,n5_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n5_butt_lng;
  input_buffer_length++;
}
void add_n6(void)
{
  add_to_str((U8*)n6_butt_txt,input_buffer,n6_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n6_butt_lng;
  input_buffer_length++;
}
void add_n7(void)
{
  add_to_str((U8*)n7_butt_txt,input_buffer,n7_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n7_butt_lng;
  input_buffer_length++;
}
void add_n8(void)
{
  add_to_str((U8*)n8_butt_txt,input_buffer,n8_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n8_butt_lng;
  input_buffer_length++;
}
void add_n9(void)
{
  add_to_str((U8*)n9_butt_txt,input_buffer,n9_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+n9_butt_lng;
  input_buffer_length++;
}

void add_dot(void)
{
  add_to_str((U8*)dot_butt_txt,input_buffer,dot_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+dot_butt_lng;
  input_buffer_length++;
}

void add_E(void)
{
  add_to_str((U8*)E_butt_txt,input_buffer,E_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+E_butt_lng;
  input_buffer_length++;
}

void add_pow(void)
{
  add_to_str((U8*)pow_butt_txt,input_buffer,pow_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+pow_butt_lng;
  input_buffer_length++;
}

void add_dbl(void)//add ^2
{
  U8 tmp[2];
  tmp[0]=250;
  tmp[1]=0;  
  add_to_str((U8*)&tmp[0],input_buffer,dbl_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+dbl_butt_lng;
  input_buffer_length++;
}

void add_min(void)//add /-/
{
  U8 tmp[2];
  tmp[0]=2;
  tmp[1]=0;  
  add_to_str((U8*)&tmp[0],input_buffer,min_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+min_butt_lng;
  input_buffer_length++;
}

void add_pi(void)//add PI
{
  U8 tmp[2];
  tmp[0]=3;
  tmp[1]=0;  
  add_to_str((U8*)&tmp[0],input_buffer,pi_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+pi_butt_lng;
  input_buffer_length++;
}



void add_sqrt(void)//add sqrt
{
  U8 tmp[4]=" ()";
  tmp[0]=1;
 // tmp[1]=40;  
 // tmp[2]=41;
  //tmp[3]=0;
  add_to_str((U8*)&tmp[0],input_buffer,sqrt_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+sqrt_butt_lng-1;
  input_buffer_length=input_buffer_length+sqrt_butt_lng;
}

void add_brackets(void)
{
  add_to_str((U8*)brackets_butt_txt,input_buffer,brackets_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+brackets_butt_lng-1;
  input_buffer_length=input_buffer_length+brackets_butt_lng;
}

void add_exp(void)
{
  add_to_str((U8*)exp_butt_txt,input_buffer,exp_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+exp_butt_lng-1;
  input_buffer_length=input_buffer_length+exp_butt_lng;
}

void add_ln(void)
{
  add_to_str((U8*)ln_butt_txt,input_buffer,ln_butt_lng,input_buffer_cursor);
  input_buffer_cursor=input_buffer_cursor+ln_butt_lng-1;
  input_buffer_length=input_buffer_length+ln_butt_lng;
}




void clear_input_buffer(void)
{
U8 i;
for (i=0;i<50;i++){input_buffer[i]=0;}
input_buffer_cursor=0;
input_buffer_length=0;
}

void move_cursor_left(void)
{
  if (input_buffer_cursor>0){input_buffer_cursor--;}
}

void move_cursor_right(void)
{
  if (input_buffer_cursor<input_buffer_length){input_buffer_cursor++;}
}


void move_dialog_cursor_left(void)
{
  if (dialog_cursor_pos>0){dialog_cursor_pos--;}
}

void move_dialog_cursor_right(void)
{
  if (dialog_cursor_pos<3){dialog_cursor_pos++;}
}


void delete_symbol(void)
{
  if (input_buffer[input_buffer_cursor]!=0)//��� �������� ���� ������
    {
      cut_chr_from_str(input_buffer,input_buffer_cursor);
      input_buffer_length--;
    }
  else
    {
      if (input_buffer_cursor!=0)
      {
        input_buffer_cursor--;
        cut_chr_from_str((U8*)input_buffer,input_buffer_cursor);
        input_buffer_length--;
      } 
    }
 
}



  


#include "stdio.h"
#include "stdint.h"

void fill_work_buffer(U8 *txt,U8 length);

U8 solve(U8 *txt,U8 length);
void find_numbers(void);
void bracket_anlyse(void);
void fill_sub_buffer(U8 *txt,U8 length);

U8 is_num_sumbol(U8 chr);
U8 is_mem_sumbol(U8 chr);

U8 is_5_level_func(U8 chr);
U8 is_4_level_func(U8 chr);
U8 is_3_level_func(U8 chr);
U8 is_2_level_func(U8 chr);
U8 is_1_level_func(U8 chr);

U8 is_x_level_func(U8 chr,U8 x);

void solve_sub_buffer(void);
void solve_work_buffer(void);

void solve_plus(U8 pos);
void solve_minus(U8 pos);
void solve_multiply(U8 pos);
void solve_div(U8 pos);
void solve_pow(U8 pos);
void solve_dbl(U8 pos);//x^2

void solve_sin(U8 pos);
void solve_cos(U8 pos);
void solve_tg(U8 pos);
void solve_sqrt(U8 pos);

void solve_ln(U8 pos);
void solve_exp(U8 pos);


void solve_func(U8 pos);

void replace_functions(void);


//������ ���������� �������������� ���������
//�������� � IAR ��� STM8
//���������� CITIZEN / ILIASAM


#include "str_math.h"
#include "stdio.h"
#include "stdint.h"


#include "str_operate.h"//������ �� ��������
#include "math.h"
#include "hardware.h"//��� ���������� ��������
#include <ctype.h>

U8 work_buffer[50];//�����, � ������� ��������� �������������� ������
U8 work_buffer_length=0;//���������� �������� � ������

U8 sub_buffer[50];//�����, � ������� ��������� �������������� ���������
U8 sub_buffer_length=0;//���������� �������� � ������ ���������

double numbers[30];//�����, ���������� �� �������� ������
U8 work_buffer_num_count=0;//���������� ����� � ������

double memory_num[4];//������ - ���������� A B C D

U8 errors=0;

double x;//�������� ������� ��������
double y;
double z;

U8 max_bracket_levell;//������������ ������� ������ ������� ������

extern double answer;

//��������� �������� �� ������� txt, length - ����� ������� � ��������
//����� ���������� � ���������� answer
//������� ���������� 0, ���� ������ ���, ���� ��� ������

U8 solve(U8 *txt,U8 length)
{

  work_buffer_num_count=0;
  errors=0;
  config_int_clk();
  
  fill_work_buffer(txt,length);
  replace_functions();
  find_numbers();//�������� ����� ���������� ��������� - � � � ...
  bracket_anlyse();//��������� �������� ������
  if (errors!=0){config_int_clk125();return errors;}
  if (max_bracket_levell==0)//���� ������ ���
  {
    fill_sub_buffer(&work_buffer[0],work_buffer_length);
    solve_sub_buffer();
    config_int_clk125();
    if (errors!=0){config_int_clk125();return errors;} else {config_int_clk125();return 0;}
  }
  //��������� ������
  solve_work_buffer();
  config_int_clk125();
  if (errors!=0){return errors;}

  return 0;
}


void replace_functions(void)
{
//�������� ������� �� �� �����
//���������� �������� � ���������-������� (������� ������������ ����� ������)
U8 i=0;
      do
      {
        if ((work_buffer[i]==115)&&(work_buffer[i+1]==105)){cut_from_str(&work_buffer[0],i+1,2);work_buffer_length=work_buffer_length-2;}//sin
        if ((work_buffer[i]==99)&&(work_buffer[i+1]==111)){cut_from_str(&work_buffer[0],i+1,2);work_buffer_length=work_buffer_length-2;}//cos
        if ((work_buffer[i]==116)&&(work_buffer[i+1]==103)){cut_from_str(&work_buffer[0],i+1,1);work_buffer_length=work_buffer_length-1;}//tg
        if ((work_buffer[i]==108)&&(work_buffer[i+1]==110)){cut_from_str(&work_buffer[0],i+1,1);work_buffer_length=work_buffer_length-1;}//ln
        if ((work_buffer[i]==101)&&(work_buffer[i+1]==120)){replace_by_char(&work_buffer[0],i,3,120);work_buffer_length=work_buffer_length-2;}//exp to x ����� e ����� ���������� ��� E
        i++;  
      } while (i<=work_buffer_length-1);
}



void find_numbers(void)
{
  //���� � ������� ������(������) �����
  //��������������� �� � double, �������� �� � ������ numbers[]
  //� �������� �� � ������� ��������� �����(�,�,�,�.....)
  //����� �������� ������� ��������� ������ (A B C D)
  
  U8 i=0;
  U8 chr=0;
  char *end_ptr;//��������� �� ����� �����
  uint16_t str_begin;
  uint16_t str_end;
  uint16_t lng;
  U8 found=0;
  U8 tmp=42;//chr - "*"
  do
  {   
      found=0;
      i=0;
      do
      {
        chr=work_buffer[i];
        i++;
      } while ((isdigit(chr)==0)&&(i<=work_buffer_length-1)&&(is_mem_sumbol(chr)==0)&&(chr!=3));//���� �����, ����� ������ ��� ��
      
      if (chr==3)//���� ������� ��
      {
        numbers[work_buffer_num_count]=3.141592654;
        work_buffer_num_count++;
        replace_by_char(&work_buffer[0],i-1,1,191+work_buffer_num_count);
        if (is_num_sumbol(work_buffer[i-2])!=0)//���� ����� �� ����� �����
        {
            add_to_str(&tmp,work_buffer,1,i-1);
            work_buffer_length++;
        }
        found=1;
      }
      
      if (is_mem_sumbol(chr)!=0)//���� ������� ������ ��������
      {
        numbers[work_buffer_num_count]=memory_num[chr-65];
        work_buffer_num_count++;
        replace_by_char(&work_buffer[0],i-1,1,191+work_buffer_num_count);
        found=1;
      }
  
      if (isdigit(chr)!=0)//�������� �� ������ ���� ����� ���������� � ����� �� �������
      {
        numbers[work_buffer_num_count]=strtod((const char *)&work_buffer[i-1],&end_ptr);
        work_buffer_num_count++;
  
        str_begin=(uint16_t)&work_buffer[i-1];
        str_end=(uint16_t)end_ptr;
        lng=str_end-str_begin;//����� ����� � ��������
        replace_by_char(&work_buffer[0],i-1,(U8)lng,191+work_buffer_num_count);//192 - ����� '�' � ASCII
        work_buffer_length=work_buffer_length-(U8)lng+1;
        if ((work_buffer[i-2]==2)&&(i>1))//�������� �� ����� (��� ��� - 2)
        {
          numbers[work_buffer_num_count-1]=numbers[work_buffer_num_count-1]*(-1);
          cut_chr_from_str(&work_buffer[0],i-2);//������� �����
          work_buffer_length--;          
        }
        found=1;
      }//end if

    } while(found==1);//�� ��� ���, ���� ���-�� ���������
}





void fill_work_buffer(U8 *txt,U8 length)
{
U8 i;
//��������� ����� ������� �� ������� ������
  for (i=0;i<length;i++)
  {
    work_buffer[i]=txt[i];
  } 
  work_buffer[length+1]=0;
  work_buffer_length=length;
  
  if (length==0) {work_buffer[0]=48;work_buffer_length=1;}//��������� "0" � ������ ������
}

void fill_sub_buffer(U8 *txt,U8 length)
{
U8 i;
//��������� ����� ������� �� ������� ������
  for (i=0;i<length;i++)
  {
    sub_buffer[i]=txt[i];
  } 
  sub_buffer[length+1]=0;
  sub_buffer_length=length;
  
}

void solve_sub_buffer(void)
{
//���� ����� �� �������� ������.
  U8 i;
  U8 chr;
  U8 found=0;
  U8 level=1;
  
  do
  {
    do//�������� ������, ���� ���������� ������� ������� ������
    {
        found=0;
        i=0;
        do//�������� ������� ������ � �������� ������� X ������
        {
          chr=sub_buffer[i];
          i++;
          if (is_x_level_func(chr,level)==1){solve_func(i-1);found=1;}//���� ������� �������, ��������� ��
        } while ((i<=sub_buffer_length-1)&&(found==0)&&(errors==0));
    }while ((found==1)&&(errors==0));
    level++;//���� �� ���� ��������� ������� ������ ������ ������ ���
  }while ((level<6)&&(errors==0));
  
  if (errors!=0){return;}
  
  if (sub_buffer_length>1){errors=2;return;}
  if (is_num_sumbol(sub_buffer[0])==1) {answer=numbers[sub_buffer[0]-192];}else {errors=4;return;}
      
}


void solve_work_buffer(void)
{
  U8 i;
  U8 chr;

  
  U8 sub_begin=0;//������ ���������
  U8 sub_end=0;//����� ���������
  U8 bracket_cnt=0;

    do
    {
        i=0;
        bracket_cnt=0;
        do//�������� ������� ������ � �������� ������
        {
          chr=work_buffer[i];
          i++;
          if (chr==40){bracket_cnt++;}
          if (chr==41){bracket_cnt--;}
          if (bracket_cnt==max_bracket_levell)//���� ������ ������������ ������
          {
            sub_begin=i;
            while (work_buffer[i]!=41){i++;}//���� ����������� ������
            sub_end=i;
            fill_sub_buffer(&work_buffer[sub_begin],sub_end-sub_begin);
            solve_sub_buffer();
            if (errors==0)
            {
              replace_by_char(&work_buffer[0],sub_begin-1,(U8)(sub_end-sub_begin+2),sub_buffer[0]);
              work_buffer_length=work_buffer_length-(sub_end-sub_begin+1);
              //found=1;
              i=sub_begin;
              bracket_cnt--;
            }
          }
         
        } while ((i<=work_buffer_length-1)&&(errors==0));
        max_bracket_levell--;
    }while ((max_bracket_levell>0)&&(errors==0));
    
  if (errors!=0){return;}
    
  if (work_buffer_length>1)//���� ������ ���
  {
    fill_sub_buffer(&work_buffer[0],work_buffer_length);
    solve_sub_buffer();
    if (is_num_sumbol(sub_buffer[0])==1) {answer=numbers[sub_buffer[0]-192];}else {errors=4;return;}
  }
  else
  {
    if (is_num_sumbol(work_buffer[0])==1) {answer=numbers[work_buffer[0]-192];}else {errors=4;return;}
  }
      
}











void solve_func(U8 pos)
{
//�������� ������� ������������� �� ��������� �������

  switch (sub_buffer[pos])
  {
  case 43:  solve_plus(pos);break;
  case 45:  solve_minus(pos);break;
  case 42:  solve_multiply(pos);break;
  case 94:  solve_pow(pos);break;
  case 250: solve_dbl(pos);break;
  case 47:  solve_div(pos);break;
  case 115: solve_sin(pos);break;
  case 99:  solve_cos(pos);break;
  case 116: solve_tg(pos);break;
  case 1:   solve_sqrt(pos);break;
  case 108: solve_ln(pos);break;
  case 120: solve_exp(pos);break;
  default: break;
    
  }
}



void solve_plus(U8 pos)
{
  //��������, ����� ������� ����� � ������ �� + ���� ��������� (� � � �)
  if ((is_num_sumbol(sub_buffer[pos-1])==1)&&(is_num_sumbol(sub_buffer[pos+1])==1))
  {
    x=numbers[sub_buffer[pos-1]-192];
    y=numbers[sub_buffer[pos+1]-192];
    z=x+y;
    numbers[sub_buffer[pos-1]-192]=z;
    
    cut_from_str(&sub_buffer[0],pos,2);
    sub_buffer_length=sub_buffer_length-2;
    work_buffer_num_count--;
    
  }
  else {errors=1;}
}

void solve_minus(U8 pos)
{
  if ((is_num_sumbol(sub_buffer[pos-1])==1)&&(is_num_sumbol(sub_buffer[pos+1])==1))
  {
    x=numbers[sub_buffer[pos-1]-192];
    y=numbers[sub_buffer[pos+1]-192];
    z=x-y;
    numbers[sub_buffer[pos-1]-192]=z;
    
    cut_from_str(&sub_buffer[0],pos,2);
    sub_buffer_length=sub_buffer_length-2;
    work_buffer_num_count--;
    
  }
  else {errors=1;}
}

void solve_multiply(U8 pos)
{
  if ((is_num_sumbol(sub_buffer[pos-1])==1)&&(is_num_sumbol(sub_buffer[pos+1])==1))
  {
    x=numbers[sub_buffer[pos-1]-192];
    y=numbers[sub_buffer[pos+1]-192];
    z=x*y;
    numbers[sub_buffer[pos-1]-192]=z;
    
    cut_from_str(&sub_buffer[0],pos,2);
    sub_buffer_length=sub_buffer_length-2;
    work_buffer_num_count--;
  }
  else {errors=1;}
}

void solve_div(U8 pos)
{
  if ((is_num_sumbol(sub_buffer[pos-1])==1)&&(is_num_sumbol(sub_buffer[pos+1])==1))
  {
    x=numbers[sub_buffer[pos-1]-192];
    y=numbers[sub_buffer[pos+1]-192];
    if (y==0) {errors=3;return;}
    z=x/y;
    numbers[sub_buffer[pos-1]-192]=z;
    
    cut_from_str(&sub_buffer[0],pos,2);
    sub_buffer_length=sub_buffer_length-2;
    work_buffer_num_count--;
  }
  else {errors=1;}
}

void solve_pow(U8 pos)
{
  if ((is_num_sumbol(sub_buffer[pos-1])==1)&&(is_num_sumbol(sub_buffer[pos+1])==1))
  {
    x=numbers[sub_buffer[pos-1]-192];
    y=numbers[sub_buffer[pos+1]-192];
    z=pow(x,y);
    numbers[sub_buffer[pos-1]-192]=z;
    
    cut_from_str(&sub_buffer[0],pos,2);
    sub_buffer_length=sub_buffer_length-2;
    work_buffer_num_count--;
  }
  else {errors=1;}
}

void solve_dbl(U8 pos)//x^2
{
  if (is_num_sumbol(sub_buffer[pos-1])==1)
  {
    x=numbers[sub_buffer[pos-1]-192];
    z=pow(x,2);      
    numbers[sub_buffer[pos-1]-192]=z;
    cut_from_str(&sub_buffer[0],pos,1);
    sub_buffer_length=sub_buffer_length-1;
    work_buffer_num_count--;
  }
  else {errors=1;}
}

void solve_sin(U8 pos)
{
  if (is_num_sumbol(sub_buffer[pos+1])==1)
  {
    x=numbers[sub_buffer[pos+1]-192];
    z=sin(x);      
    numbers[sub_buffer[pos+1]-192]=z;
    cut_from_str(&sub_buffer[0],pos,1);
    sub_buffer_length=sub_buffer_length-1;
    work_buffer_num_count--;
  }
  else {errors=1;}
}

void solve_cos(U8 pos)
{
  if (is_num_sumbol(sub_buffer[pos+1])==1)
  {
    x=numbers[sub_buffer[pos+1]-192];
    z=cos(x);      
    numbers[sub_buffer[pos+1]-192]=z;
    cut_from_str(&sub_buffer[0],pos,1);
    sub_buffer_length=sub_buffer_length-1;
    work_buffer_num_count--;
  }
  else {errors=1;}
}

void solve_tg(U8 pos)
{
  if (is_num_sumbol(sub_buffer[pos+1])==1)
  {
    x=numbers[sub_buffer[pos+1]-192];
    z=tan(x);      
    numbers[sub_buffer[pos+1]-192]=z;
    cut_from_str(&sub_buffer[0],pos,1);
    sub_buffer_length=sub_buffer_length-1;
    work_buffer_num_count--;
  }
  else {errors=1;}
}

void solve_sqrt(U8 pos)
{
  if (is_num_sumbol(sub_buffer[pos+1])==1)
  {
    x=numbers[sub_buffer[pos+1]-192];
    if (x<0){errors=6;return;}
    z=sqrt(x);      
    numbers[sub_buffer[pos+1]-192]=z;
    cut_from_str(&sub_buffer[0],pos,1);
    sub_buffer_length=sub_buffer_length-1;
    work_buffer_num_count--;
  }
  else {errors=1;}
}

void solve_exp(U8 pos)
{
  if (is_num_sumbol(sub_buffer[pos+1])==1)
  {
    x=numbers[sub_buffer[pos+1]-192];
    z=exp(x);      
    numbers[sub_buffer[pos+1]-192]=z;
    cut_from_str(&sub_buffer[0],pos,1);
    sub_buffer_length=sub_buffer_length-1;
    work_buffer_num_count--;
  }
  else {errors=1;}
}

void solve_ln(U8 pos)
{
  if (is_num_sumbol(sub_buffer[pos+1])==1)
  {
    x=numbers[sub_buffer[pos+1]-192];
    if (x<0){errors=7;return;}
    z=log(x);      
    numbers[sub_buffer[pos+1]-192]=z;
    cut_from_str(&sub_buffer[0],pos,1);
    sub_buffer_length=sub_buffer_length-1;
    work_buffer_num_count--;
  }
  else {errors=1;}
}







U8 is_num_sumbol(U8 chr)
{
  //���������� 1 ���� ������ ��������� � ������ (� � � ...)
  if ((chr>191)&&(chr<212)){return 1;}
  return 0;
}

U8 is_mem_sumbol(U8 chr)
{
  //���������� 1 ���� ������ ��������� � �������� ��������� ������ (A B C D)
  if ((chr>64)&&(chr<69)){return 1;}
  return 0;
}



U8 is_x_level_func(U8 chr,U8 x)
{
U8 tmp=0;  
  switch (x)
  {
  case 5: tmp=is_5_level_func(chr);break;
  case 4: tmp=is_4_level_func(chr);break;
  case 3: tmp=is_3_level_func(chr);break;
  case 2: tmp=is_2_level_func(chr);break;
  case 1: tmp=is_1_level_func(chr);break;
  }
return tmp; 
}




U8 is_5_level_func(U8 chr)
{
  //���������� 1 ���� + ��� -
  if ((chr==43)||(chr==45)){return 1;}
  return 0;
}

U8 is_4_level_func(U8 chr)
{
  //���������� 1 ���� * ��� /
  if ((chr==42)||(chr==47)){return 1;}
  return 0;
}

U8 is_3_level_func(U8 chr)
{
  //���������� 1 ���� ^
  if (chr==94){return 1;}
  return 0;
}

U8 is_2_level_func(U8 chr)
{
  //���������� 1 ���� ^2
  if (chr==250){return 1;}
  return 0;
}

U8 is_1_level_func(U8 chr)
{
  //���������� 1 ���� c ��� s ��� t ��� sqrt ��� l ��� x
  if ((chr==115)||(chr==99)||(chr==116)||(chr==1)||(chr==108)||(chr==120)){return 1;}
  return 0;
}




void bracket_anlyse(void)
{
  //���������, ����� ���������� ������ ���� ������ � ��������� ���� ������� ������
  
  U8 i;
  U8 bracket_cnt=0;
  max_bracket_levell=0;
  
  for (i=0;i<work_buffer_length;i++)
  {
    if (work_buffer[i]==40){bracket_cnt++;}
    if (work_buffer[i]==41){bracket_cnt--;}
    if (bracket_cnt>max_bracket_levell){max_bracket_levell=bracket_cnt;}
  }
  if (bracket_cnt!=0){errors=5;}
}



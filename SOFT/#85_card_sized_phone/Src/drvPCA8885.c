#include "board.h"
#include "drvPCA8885.h"
#include "halI2C.h"


#if (I2C_PCA8885 == 1)

#define I2C_PCA8885_COM_SOFT_START  	0x00
#define I2C_PCA8885_COM_WAKE_UP         0x06
#define I2C_PCA8885_COM_WRITE_CONFIG    0x30
#define I2C_PCA8885_COM_WRITE_CLOCK     0x35
#define I2C_PCA8885_COM_WRITE_MASK      0x39


MSG drvPCA8885_init (U8 newstate)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[5];
    
    I2C_buf[0] = I2C_PCA8885_COM_SOFT_START;
    if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_PCA8885_ADDRESS, &I2C_buf[0], 1, 0))
    {
    	I2C_buf[0] = I2C_PCA8885_COM_WAKE_UP;
		if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_PCA8885_ADDRESS, &I2C_buf[0], 1, 0))
		{
			I2C_buf[0] = I2C_PCA8885_COM_WRITE_CONFIG;
			I2C_buf[1] = 0x0B; //SWM + KM[1:0]-2-key mode + INTM + MSKMODE
			if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_PCA8885_ADDRESS, &I2C_buf[0], 2, 0))
			{
				I2C_buf[0] = I2C_PCA8885_COM_WRITE_CLOCK;
				I2C_buf[1] = 0x1F; //MAX, default - 0x0C
				if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_PCA8885_ADDRESS, &I2C_buf[0], 2, 0))
				{
					I2C_buf[0] = I2C_PCA8885_COM_WRITE_MASK;
					I2C_buf[1] = 0xFF; //channels all on
					if (FUNCTION_RETURN_OK == halI2C_transmit (I2C_PCA8885_ADDRESS, &I2C_buf[0], 2, 0))
					{
						respond = FUNCTION_RETURN_OK;
					}
				}
			}
		}
    }

    return respond;
}


MSG drvPCA8885_get (U8 *key)
{
    MSG respond = FUNCTION_RETURN_ERROR;
    U8 I2C_buf[1];
    
    *key = HKEY_NOT_KEY;
	if (FUNCTION_RETURN_OK == halI2C_receive (I2C_PCA8885_ADDRESS, &I2C_buf[0], 1, 0))
	{
        //if ((I2C_buf[0] & 0x0F) && (I2C_buf[0] & 0xF0))
        {
            switch (I2C_buf[0])
            {
            case HKEY_0: 
            case HKEY_1:
            case HKEY_2:
            case HKEY_3:
            case HKEY_4:
            case HKEY_5:
            case HKEY_6:
            case HKEY_7:
            case HKEY_8:
            case HKEY_9:
            case HKEY_DOT:
            case HKEY_EQUAL:
            case HKEY_UP:
            case HKEY_DOWN:
            case HKEY_LEFT:
            case HKEY_RIGHT:
                *key = I2C_buf[0];
                respond = FUNCTION_RETURN_OK;
                break;
            
            default: break;
            }
        }
	}

    return respond;
}


#endif

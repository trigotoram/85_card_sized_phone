/**
 * @file    hal.h
 * @author  Ht3h5793, HTL1
 * @date    44.14.2018
 * @version V0.0.1
 * @brief   
 */

#ifndef HAL_H
#define HAL_H 20180405

#include "board.h"

#include "modRTC.h"
     
     
#ifdef	__cplusplus
extern "C" {
#endif

void drvBUZZER_init (void);

void drvBUZZER_peep (U8 numbers);

void modRTC_cnt_get (rtc_s *rtc);

void modRTC_cnt_set (rtc_s *rtc);

U8 modRTC_init (void);

void halSPI2_GPIO_SPI_init (void);

U8 halSPI2_xspi (U8 data);
U8 halSPI2_mxspi (U8 data, U32 num);

void halADC_init (void);

void halADC_deinit (void);

U16 halADC_get_BATT_mV (void);

void hal_vibro_ON (void);

void hal_vibro_OFF (void);

void hal_ESP8266_ON (void);

void hal_ESP8266_OFF (void);

void hal_SIM800_ONOFF (void);


// for Menu
void RTC_year_set (S8 t);

void RTC_year_get (char *str);

void RTC_month_set (S8 t);

void RTC_month_get (char *str);

void RTC_day_set (S8 t);

void RTC_day_get (char *str);

void RTC_hour_set (S8 t);

void RTC_hour_get (char *str);

void RTC_minute_set (S8 t);

void RTC_minute_get (char *str);

void RTC_second_set (S8 t);

void RTC_second_get (char *str);


void RTC_redact (S8 t);

void RTC_save (S8 t);



void contrast_set (S8 t);

void contrast_get (char *str);

void contrast_save (S8 t);

     
     
     
     
#ifdef	__cplusplus
}
#endif

#endif /* HAL_H */

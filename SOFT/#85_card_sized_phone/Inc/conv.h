/**
 * @file    conv.h
 * @author  s.aksenov, Ht3h5793, CD45
 * @date    13.01.2013
 * @version V2.1.0
 * @brief   ��������������� ������, ������ �� ��������
 * @todo    ��������� �� ������������ �������� � ����� �� ����!!! (����� �������������
 *
 */

#ifndef CONV_H
#define	CONV_H  20180403

#include "board.h"

#ifdef	__cplusplus
extern "C" {
#endif

// Float � Int @todo
    
//U16 _strlen (const char *str);
    
//U8 U32_2_str (char *pstring, U32 value);
//U8 U8_2_str (char *pstring, U8 value);


S32 float2int32 (FLOAT number, U8 zeros);
char *_strncmp (char *strA, char *strB);
char *skip_space (char *ptr);
    
//return hex number in range 0-0xf or error 0xff if char is not a hexadecimal digit
U8 char2hex (char c);

U8 bcd2dec (U8 val);
U8 dec2bcd (U8 val);



#ifdef	__cplusplus
}
#endif

#endif	/* CONV_H */

/*
 * @file
 * @author  Ht3h5793
 * @date    04.04.2014
 * @version V0.0.1
 * @brief
 *
*/

#ifndef MODMENU_H
#define	MODMENU_H 20180405 /* Revision ID */

#include "board.h"
#include "modPaint.h"


typedef struct cursor_t_ {
    U8    cursor;
    U16   cursorPos_x;
    U16   cursorPos_y;
} cursor_t;

typedef struct style_t_ {
    COLOR    bColor;
    COLOR    borderColor;
    COLOR    fontColor;
} style_t;

typedef struct menu_t_ {
    U8    state;
    U16   position;
//        uS16   numPrev;
//        uS16   numnext;
    U16   drawPos;

    style_t style;
    BOOL change;// = FALSE;
    
    SYSTIME key_hold_time;
} menu_t;


/* Flags for MT_FUNCTION_CALL */
#define MENU_PROP_EDIT  0x0100

enum EDITABLE {
    NO_EDIT = 0,
    EDIT_BOOL = 1,
    EDIT_U8 = 2,
    EDIT_U16 = 3,
    EDIT_U32 = 4,
    EDIT_S8 = 5,
    EDIT_S16 = 6,
    EDIT_S32 = 7,
    EDIT_FLOAT = 8,
    EDIT_DOUBLE = 9,
    EDIT_STRING = 10,
    EDIT_CHAR = 11,

    //MENU_EXIggT = 12
};

//    signed char numInStringArray; /** ��������� �� ������ ������ */
//    signed char prevItem; /** ���������� ������� */
//    signed char subMenu;  /** ��������� ���� */
//    uS16 numInDataBase; /** ����� ��������� � �� */
//    unsigned char editable; /** ������ ��������� (������� / �� �������) */
typedef struct menuItem_t_ { // ��������� ����
    void        *next; // ��������� ������� ����
    void        *previous; // ����������
    void        *child; // ���� � �������
    void        *parent; // ����� �� ����
    //U8     editable; // ������������� �� ��������?
    void        *value;   // ��� ��������
    void        *callback;
    const char  *text; // ����� ��� ������������

} menuItem_t;


typedef enum {
    MENU_STYLE_NOSELECT = 0,
    MENU_STYLE_SELECT,
    MENU_STYLE_BORDER,

} tMENU_STYLE;


#ifdef	__cplusplus
extern "C" {
#endif
void    menu_init (U8 );
MSG     menu_run (void);

//private
void    menu_setStyle (U8 st);
void    menu_redrawAll (void);
void    menu_redrawHeader (const char *str);
MSG     menu_setMenu (S8 menuPos, const char *str);

void    dummy (void);

void    dummyStr (char *pStr); //return zero string


#ifdef	__cplusplus
}
#endif

#endif	/* MODMENU_H */

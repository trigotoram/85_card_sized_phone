/**
 * @file    modKey_local.h
 * @author  Ht3h5793, HTL1
 * @date    16.11.2016
 * @version V0.0.1
 * @brief   ��� ����� ��������� ������
 */

#ifndef MODKEY_LOCAL_H
#define MODKEY_LOCAL_H 20170807

#include "board.h"


/** ����������� �������� �������� ������, 
 *  ������ ������ 0!
 */
#define HALKEY_COUNTER_V_MIN            1

/** ����������� �������� �������� ������ 
 * ������� � ���, ��� ������������ �������� ���������� ����������!
*/
#define HALKEY_COUNTER_V_MAX            (HALKEY_COUNTER_V_MIN + 5)

/** �������� �������� ��� ������� ��������� ��� ������ ������ */
#define MODKEY_COUNTER_VALUE_ON         (3)   // 90% �� ������������� ��������
/** �������� �������� ��� ������� ��������� ��� ������ �������� */
#define MODKEY_COUNTER_VALUE_OFF        (2)  //

/** ����� �� ���������  ��������� ������, ��*/
#define MODKEY_DEFAULT_HOLD_TIME_PERIOD 1000
/** ����� �� ���������  ������ ������, ���� ����� �� �������, ��*/
#define MODKEY_DEFAULT_REALISED_TIMEOUT 5000


#endif /* MODKEY_LOCAL_H */

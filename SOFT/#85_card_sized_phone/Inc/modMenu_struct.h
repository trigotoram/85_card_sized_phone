/**
 * @file    hal.h
 * @author  Ht3h5793, HTL1
 * @date    44.14.2018
 * @version V0.0.1
 * @brief   
 */

#ifndef MODMENU_STRUCT_H
#define MODMENU_STRUCT_H 20180406

#include "board.h"
#include "hal.h" //��� ������� ��� ���������������
#include "modMenu.h" //��� ������� ��� ���������������


////        +-----------------------------------------------------------  ����� �������� ����. ��-����������� �������!
////        |           +---------------------------------------------------  �������� ��������� ����.
////        |           |      +---------------------------------------------  ��������� ������� ����.
////        |           |      |      +----------------------------------------  ���������� ������� ����.
////        |           |      |      |      +------------------------------------  ��������� ���� (-1 - �������� � db(������������ �������� ����������)). ��������� �������� ��������� ����, ���� �� ����� ����������� ��������
////        |           |      |      |      |              +------------------------------  ����� ��������� � database (enum).
////        |           |      |      |      |              |      +------------  ����������� ���������: 1 - �������������, 0 - �� �������������.
////        |           |      |      |      |              |      |

//           CURRENT         NEXT,        PREVIOUS          CHILD           PARENT,    value, callback,  text

MAKE_MENU(   MENU_P1,      MENU_P2,     NULL_ENTRY,     MENU_P1_1,      NULL_ENTRY,  dummyStr,    dummy, "Test");
MAKE_MENU(   MENU_P2,      MENU_P3,     MENU_P1,        MENU_P2_1,      NULL_ENTRY,  dummyStr,    dummy, "System");
MAKE_MENU(   MENU_P3,      MENU_P4,     MENU_P2,        MENU_P3_1,      NULL_ENTRY,  dummyStr,    dummy, "Screensavers");
MAKE_MENU(   MENU_P4,      MENU_P5,     MENU_P3,        MENU_P4_1,      NULL_ENTRY,  dummyStr,    dummy, "Apps");
MAKE_MENU(   MENU_P5,      MENU_P6,     MENU_P4,        MENU_P5_1,      NULL_ENTRY,  dummyStr,    dummy, "Games");
MAKE_MENU(   MENU_P6,      MENU_P7,     MENU_P5,        MENU_P6_1,      NULL_ENTRY,  dummyStr,    dummy, "WIFI module");
MAKE_MENU(   MENU_P7,      MENU_P8,     MENU_P6,        MENU_P7_1,      NULL_ENTRY,  dummyStr,    dummy, "Call");
MAKE_MENU(   MENU_P8,      MENU_P9,     MENU_P7,        MENU_P8_1,      NULL_ENTRY,  dummyStr,    dummy, "Settings ");
MAKE_MENU(   MENU_P9,      NULL_ENTRY,  MENU_P8,        MENU_P9_1,      NULL_ENTRY,  dummyStr,    dummy, "About ");


// ������� 1
MAKE_MENU( MENU_P1_1,    MENU_P1_2,     NULL_ENTRY,     MENU_P1,        MENU_P1,   dummyStr,      dummy,   "test1");;
MAKE_MENU( MENU_P1_2,    NULL_ENTRY,    MENU_P1_1,      MENU_P1,        MENU_P1,   dummyStr,      dummy, "test2 ");


// ������� 2
MAKE_MENU( MENU_P2_1,    NULL_ENTRY,     NULL_ENTRY,    MENU_P2,        MENU_P2,   dummyStr,      sys_run, "Info");


// ������� 3
MAKE_MENU( MENU_P3_1,    MENU_P3_2,     NULL_ENTRY,     MENU_P3,        MENU_P3,   dummyStr, screen_saver_mosaique_run, "Mosaique");
MAKE_MENU( MENU_P3_2,    MENU_P3_3,     MENU_P3_1,      MENU_P3,        MENU_P3,   dummyStr, screen_saver_life_run, "Life");
MAKE_MENU( MENU_P3_3,    MENU_P3_4,     MENU_P3_2,      MENU_P3,        MENU_P3,   dummyStr, screen_saver_3D_run, "3D");
MAKE_MENU( MENU_P3_4,    NULL_ENTRY,    MENU_P3_3,      MENU_P3,        MENU_P3,   dummyStr, screen_saver_random_run, "Random");

// ������� 4
MAKE_MENU( MENU_P4_1,     NULL_ENTRY,    NULL_ENTRY,     MENU_P4,        MENU_P4,   dummyStr, dummy, "TODO");


// ������� 5
MAKE_MENU( MENU_P5_1,     MENU_P5_2,    NULL_ENTRY,     NULL_ENTRY,        MENU_P5,   dummyStr, game_worm, "Worm");
MAKE_MENU( MENU_P5_2,    NULL_ENTRY,    MENU_P5_1,      NULL_ENTRY,        MENU_P5,   dummyStr, dummy, "Tetris");

// ������� 6
MAKE_MENU( MENU_P6_1,     NULL_ENTRY,    NULL_ENTRY,     MENU_P6,        MENU_P5,   WIFI_get,    WIFI_set, "STATIS:");


// ������� 7
MAKE_MENU( MENU_P7_1,     NULL_ENTRY,    NULL_ENTRY,     MENU_P7,        MENU_P7,   dummyStr, dummy, "TODO");


// ������� 8
MAKE_MENU( MENU_P8_1,     MENU_P8_2,    NULL_ENTRY,     MENU_P8_1_1,        MENU_P8,   dummyStr, dummy, "RTC settings->");
MAKE_MENU( MENU_P8_2,    NULL_ENTRY,    MENU_P8_1,      MENU_P8_2_1,        MENU_P8,   dummyStr, dummy, "OLED settings->");

// ������� 8.1
MAKE_MENU( MENU_P8_1_1,    MENU_P8_1_2,     NULL_ENTRY,      NULL_ENTRY,         MENU_P8_1,   RTC_year_get,    RTC_year_set,   "RTC->year");
MAKE_MENU( MENU_P8_1_2,    MENU_P8_1_3,     MENU_P8_1_1,     NULL_ENTRY,         MENU_P8_1,   RTC_month_get,   RTC_month_set,  "RTC->month");
MAKE_MENU( MENU_P8_1_3,    MENU_P8_1_4,     MENU_P8_1_2,     NULL_ENTRY,         MENU_P8_1,   RTC_day_get,     RTC_day_set,    "RTC->day");
MAKE_MENU( MENU_P8_1_4,    MENU_P8_1_5,     MENU_P8_1_3,     NULL_ENTRY,         MENU_P8_1,   RTC_hour_get,    RTC_hour_set,   "RTC->hour");
MAKE_MENU( MENU_P8_1_5,    MENU_P8_1_6,     MENU_P8_1_4,     NULL_ENTRY,         MENU_P8_1,   RTC_minute_get,  RTC_minute_set, "RTC->minute");
MAKE_MENU( MENU_P8_1_6,    MENU_P8_1_7,     MENU_P8_1_5,     NULL_ENTRY,         MENU_P8_1,   RTC_second_get,  RTC_second_set, "RTC->second");
MAKE_MENU( MENU_P8_1_7,    NULL_ENTRY,      MENU_P8_1_6,     NULL_ENTRY,         MENU_P8_1,   dummyStr,  RTC_save, "Save ");

// ������� 8.2
MAKE_MENU( MENU_P8_2_1,    MENU_P8_2_2,      NULL_ENTRY,     MENU_P8_2,         MENU_P8_2,   contrast_get, contrast_set, "��������");
MAKE_MENU( MENU_P8_2_2,    NULL_ENTRY,      MENU_P8_2_1,     MENU_P8_2,         MENU_P8_2,   dummyStr, contrast_save, "Save ");



// ������� 9
MAKE_MENU( MENU_P9_1,    NULL_ENTRY,      NULL_ENTRY,     MENU_P9,         MENU_P9,   dummyStr, dummy, "by @httri 20180406");




#endif /* MODMENU_STRUCT_H */

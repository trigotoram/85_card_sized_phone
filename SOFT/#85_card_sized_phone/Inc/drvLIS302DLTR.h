/**
 * @file    drvLIS302DLTR.h
 * @author  httri
 * @date    07.01.2018  8.19
 * @version V1.0.0
 * @brief 


 */
 

#ifndef DRVLIS302DL_H
#define	DRVLIS302DL_H 20180107

#include "board.h"


#define LIS302DL_REG_WHOAMI               0x0F
//#define LIS3DH_REG_TEMPCFG              0x1F
#define LIS302DL_REG_CTRL1                0x20
#define LIS302DL_REG_CTRL2                0x21
#define LIS302DL_REG_CTRL3                0x22
//#define LIS3DH_REG_CTRL4                0x23
//#define LIS3DH_REG_CTRL5                0x24
//#define LIS3DH_REG_CTRL6                0x25
#define LIS3DH_REG_REFERENCE            0x26
#define LIS302DL_REG_STATUS              0x27
//#define LIS302DL_REG_OUT_X_L              0x28
#define LIS302DL_REG_OUT_X              0x29
//#define LIS302DL_REG_OUT_Y_L              0x2A
#define LIS302DL_REG_OUT_Y              0x2B
//#define LIS302DL_REG_OUT_Z_L              0x2C
#define LIS302DL_REG_OUT_Z              0x2D
//#define LIS3DH_REG_FIFOCTRL             0x2E
//#define LIS3DH_REG_FIFOSRC              0x2F
#define LIS302DL_FF_WU_CFG_1_REG             0x30
#define LIS3DH_REG_INT1SRC              0x31
#define LIS3DH_REG_INT1THS              0x32
#define LIS3DH_REG_INT1DUR              0x33

#define LIS302DL_REG_CLICKCFG             0x38
#define LIS302DL_REG_CLICKSRC             0x39
#define LIS302DL_REG_CLICK_THSY_X         0x3B
#define LIS302DL_REG_CLICK_THSZ         0x3C
#define LIS302DL_REG_TIMELIMIT            0x3D
#define LIS302DL_REG_TIMELATENCY          0x3E
#define LIS302DL_REG_TIMEWINDOW           0x3F


typedef enum
{
  LIS3DH_RANGE_8_G                      = 0x2,   // +/- 8g
  LIS3DH_RANGE_4_G                      = 0x1,   // +/- 4g
  LIS3DH_RANGE_2_G                      = 0x0    // +/- 2g (default value)
} lis3dh_range_t;

typedef enum
{
  LIS3DH_AXIS_X                         = 0x0,
  LIS3DH_AXIS_Y                         = 0x1,
  LIS3DH_AXIS_Z                         = 0x2,
} lis3dh_axis_t;


/* Used with register 0x2A (LIS3DH_REG_CTRL_REG1) to set bandwidth */
typedef enum
{
  LIS3DH_DATARATE_400_HZ                = 0x7, //  400Hz 
  LIS3DH_DATARATE_200_HZ                = 0x6, //  200Hz
  LIS3DH_DATARATE_100_HZ                = 0x5, //  100Hz
  LIS3DH_DATARATE_50_HZ                 = 0x4, //   50Hz
  LIS3DH_DATARATE_25_HZ                 = 0x3, //   25Hz
  LIS3DH_DATARATE_10_HZ                 = 0x2, // 10 Hz
  LIS3DH_DATARATE_1_HZ                  = 0x1, // 1 Hz
  LIS3DH_DATARATE_POWERDOWN             = 0,
  LIS3DH_DATARATE_LOWPOWER_1K6HZ        = 0x8,
  LIS3DH_DATARATE_LOWPOWER_5KHZ         = 0x9,

} LIS302DL_dataRate_t;


#ifdef	__cplusplus
//extern "C" {
#endif

MSG    drvLIS302DL_getXYZ (U8 *x, U8 *y, U8 *z);

MSG  drvLIS302DL_init (void);

U8  drvLIS302DL_get_click (void);


#ifdef	__cplusplus
//}
#endif

#endif	/** DRVLIS302DL_H */


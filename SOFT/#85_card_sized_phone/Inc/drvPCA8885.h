/**
 * @file    drvPCA8885.h
 * @author  httri
 * @date    08.01.2017  2.30
 * @version V1.1.0
 * @brief 

 */
 

#ifndef DRVPCA8885_H
#define	DRVPCA8885_H 20180108

#include "board.h"


#ifdef	__cplusplus
extern "C" {
#endif


MSG       drvPCA8885_init (U8 newstate);

MSG       drvPCA8885_get (U8 *key);


#ifdef	__cplusplus
}
#endif

#endif	/** DRVPCA8885_H */

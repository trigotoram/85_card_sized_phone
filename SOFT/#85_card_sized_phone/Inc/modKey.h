/*
 * @file    hal LCD
 * @author  AKsenov, Ht3h5793
 * @date    05.03.2014
 * @version V6.5.2
 * @brief  
 * @todo
    
modKey_init ();
...
while (1) {
    modKey_run ();
    if (modKey_getState (KEY_UP) == MODKEY_STATE_PRESSED)
    {
        
    }
}
    

*/
#ifndef MODKEY_H
#define MODKEY_H                        20180405

#include "board.h"
#include "modKey_local.h"

// ��������� ������
typedef enum {
    MODKEY_STATE_NOT_PRESSED            = 0, /** ������ �� ������ */
    MODKEY_STATE_RELEASED               = 1, /** ������ ���� ������ � �������� */
    MODKEY_STATE_PRESSED                = 2, /** ������ ���� ������ */
    MODKEY_STATE_HOLD                   = 3, /** ������ ���� ������ � �������� */
    MODKEY_STATE_HOLD_TIME              = 4  /** ������ ���� �������� � ������� ��������� ������� ��������� */
} KEY_STATE;


#ifdef	__cplusplus
extern "C" {
#endif

/**
 * ������������� ������.
 */
void modKey_init (void);

/**
 * �������� ��������� ������ �����������
 * @param keyNumber - ����� ������
 * @return          - ��������� ������
 */
KEY_STATE modKey_getState (U8 keyNumber);

/**
 * ����������� ��������� ������
 * @param keyNumber - ����� ������.
 * @param pinState - ���������� ��������� ������(������).
 */
void modKey_run (void);

/**
 * ���������� ����� ��������� ������
 * @param holdTimePeriod ����������� ������ ��� ��������� ��������� ���������
 */
void modKey_setHoldTimePeriod (U8 keyNumber, SYSTIME holdTimePeriod);

/**
 * ���������� ����� ���������� ������
 * @param holdTimePeriod ����������� ������ ��� ��������� ��������� ���������
 */
void modKey_setReleaseTimeout (U8 keyNumber, SYSTIME releaseTimeout);


#ifdef	__cplusplus
}
#endif

#endif /* MODKEY_H */

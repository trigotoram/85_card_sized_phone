/**
 * @file    .h
 * @author  Ht3h5793
 * @date    15.12.2014
 * @version V1587.2320.10
 * @brief   
*/

#ifndef DRVSTORAGE_H
#define	DRVSTORAGE_H 20180406
/**
 *  ������ ��� "include"
 */

#include "board.h"


/**
 *  ������ ��� "define"
 */


/*
 * ����������� ������� �������
 */

/**
 *  ������ ��� "typedef"
 */

/**
 *  ������ ��� ���������� �������
 */

#ifdef	__cplusplus
extern "C" {
#endif


  
    

void drvStorage_init  (void);

// set default settings
void drvStorage_restore (void);

// ���������� �������� �� ��������������� ��������� ---------------
void drvStorage_read (void);

// ������ �������� � �������������� ��������� ---------------------
void drvStorage_save ();




    
  /**
  * @brief  This function programs a 16-bit word.
  *         The Programming bit (PG) is set at the beginning and reset at the end
  *         of the function, in case of successive programming, these two operations
  *         could be performed outside the function.
  *         This function waits the end of programming, clears the appropriate bit in 
  *         the Status register and eventually reports an error. 
  * @param  flash_addr is the address to be programmed
  *         data is the 16-bit word to program
  * @retval None
  */
MSG   halFLASH_FlashWord16Prog (uint32_t flash_addr, uint16_t data);
     
     
     
     
     
     
//data - ��������� �� ������������ ������
//address - ����� �� flash
//count - ���������� ������������ ����, ������ ���� ������ 2
MSG   halEEPROM_write (uint16_t address, uint8_t *data, uint16_t cnt);


/**
 * 
 * @param adress - ������ � ������, �������� 65565 �����
 * @param pBuf   - ��������� �� �����
 * @param numBuf - ����� ����, �������� 32
 * @return I2C_FUNCTION_RETURN_READY         - ������, ������ ����������
 *         I2C_FUNCTION_RETURN_ERROR_TIMEOUT - ���-�� ������, ��� ��������� ��������� ����� ����������� ��������� ������
 */
uint8_t     halFLASH_read8 (uint32_t address);
uint16_t    halFLASH_read16 (uint32_t address);
uint32_t    halFLASH_read32 (uint32_t address);

MSG       halFLASH_read (uint16_t address, uint8_t *data, uint16_t cnt);



/**
  * @brief  This function prepares the flash to be erased or programmed.
  *         It first checks no flash operation is on going,
  *         then unlocks the flash if it is locked.
  * @param  None
  * @retval None
  */
void    halFLASH_unlock (void);
void    halFLASH_lock (void);

    
/**
  * @brief  This function erases a page of flash.
  *         The Page Erase bit (PER) is set at the beginning and reset at the end
  *         of the function, in case of successive erase, these two operations
  *         could be performed outside the function.
  * @param  page_addr is an address inside the page to erase
  * @retval None
  */
MSG   halFLASH_erase_page (uint32_t );


/**
  * @brief  This function checks that the whole page has been correctly erased
  *         A word is erased while all its bits are set.
  * @param  first_page_addr is the first address of the page to erase
  * @retval None
  */
MSG   halFLASH_erase_check (uint32_t );


void    halFLASH_erase_all (void);







#ifdef	__cplusplus
}
#endif

#endif	/* DRVSTORAGE_H */

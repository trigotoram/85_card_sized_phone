/* 
 * File:   board.h
 */

#ifndef BOARD_H
#define	BOARD_H 20170712


#define BOARD_85_card_sized_phone       (1)//������� �����
#define STM32                           1//for debug.h


/**
 *  ������ ��� "include"
 */
#include "main.h"
#include "stm32f0xx_hal.h"
#include "usb_device.h"
//#include "usbd_cdc_if.h"
#include "conv.h"
#include "build_defs.h"


/**
 *  ������ ��� "define"
 */

//#define ERROR_ACTION(a)              //fERROR_ACTION(a,__MODULE__,__LINE__)

#define SYSTIME                         uint32_t
#define TIC_PERIOD                      (1000UL) //us
#define SYS_FREQ                        (48000000UL)
#define DELAY_TIM                       TIM3
#define DELAY_INIT                      do { RCC->APB1ENR |= RCC_APB1ENR_TIM3EN; DELAY_TIM->PSC = (SYS_FREQ / 1000000UL) - 1; } while (0)

#ifndef BOOL
    #define BOOL                        uint8_t
#endif

#ifndef TRUE
    #define TRUE                        1
#endif
#ifndef FALSE
    #define FALSE                       0
#endif

#ifndef NULL
    #define NULL                        (0)
#endif

#define FUNCTION_RETURN_OK              TRUE //- �������� ���������
#define FUNCTION_RETURN_ERROR           FALSE
#define MSG                             uint32_t
#define U8                              uint8_t
#define S8                              int8_t
#define VU8                             uint8_t
#define U16                             uint16_t
#define S16                             int16_t
#define U32                             uint32_t
#define S32                             int32_t
#define U64                             uint64_t
#define S64                             int64_t
#define FLOAT                           float
#define DOUBLE                          double
#define INT								int32_t

#define CODE							//ICACHE_FLASH_ATTR
#define ROM								//ICACHE_RODATA_ATTR			 


#ifndef LOW
    #define LOW                         0
#endif

#ifndef HIGH
    #define HIGH                        1
#endif


//Random
#define MODRANDOM_4096BIT_EN            0


// GPIO ------------------------------------------

//OLED
#define SPI2_SOFTAWARE                  0

#if SPI2_SOFTAWARE
#define GPIO_OLED_DC_L                  GPIO_OLED_DC_val = 0//GPIOB->BSRR = GPIO_BSRR_BR14
#define GPIO_OLED_DC_H                  GPIO_OLED_DC_val = 1//GPIOB->BSRR = GPIO_BSRR_BS14
#define GPIO_OLED_CS_L                  GPIOB->BSRR = GPIO_BSRR_BR_12
#define GPIO_OLED_CS_H                  GPIOB->BSRR = GPIO_BSRR_BS_12
#define OLED_xspi                       halSPI2_xspi

#else
#define GPIO_OLED_DC_L                  GPIO_OLED_DC_val = 0 //GPIOB->BSRR = GPIO_BSRR_BR_14
#define GPIO_OLED_DC_H                  GPIO_OLED_DC_val = 1 //GPIOB->BSRR = GPIO_BSRR_BS_14
#define GPIO_OLED_CS_L                  GPIOB->BSRR = GPIO_BSRR_BR_12
#define GPIO_OLED_CS_H                  do { while ((SPI2->SR & SPI_SR_BSY) != 0) { __NOP(); }; GPIOB->BSRR = GPIO_BSRR_BS_12; } while (0)
#define OLED_xspi                       halSPI2_xspi

#endif

// RTC ---------------------------------
#define MOD_RTC                         0


// Screen ------------------------------
#define HT3_PAINT                       1

//#define LCD_LPH9157                   1
#define LCD_LM15SGFN07                  0
#define SSD1306_128_64                  1

#define OLED_SPI3                       1
enum { 
    SCREEN_ORIENTATION_0 = 0, 
    SCREEN_ORIENTATION_90,
    SCREEN_ORIENTATION_180,
    SCREEN_ORIENTATION_270
};

#ifndef COORD
typedef int32_t                         COORD;
#endif

//#define PAINT_FONT_x3y5                 1
#define PAINT_FONT_Generic_8pt          1
// ;paint primitives
#define PAINT_NEED_LINE                 1
#define PAINT_NEED_RECT                 1
#define PAINT_NEED_CIRCLE               1

#define CHECK_BORDER                    0



#if SSD1306_128_64

#define COLORS_1BIT                     1
typedef U8 COLOR;

//#define LCD_ENABLE_PARTIAL_UPDATE   1

#define SCREEN_W	                    128
#define SCREEN_H	                    64
#define SCREEN_VIRTUAL_W                SCREEN_W
#define SCREEN_VIRTUAL_H                SCREEN_H

#define SCREEN_ORIENTATION_GLOBAL       SCREEN_ORIENTATION_0
#if (SCREEN_W > SCREEN_H)
    #define PAINT_BUF_SIZE              SCREEN_W
#else
    #define PAINT_BUF_SIZE              SCREEN_H
#endif

#define LCD_RES_L                       //HAL_GPIO_WritePin(LCD_RES_GPIO_Port, LCD_RES_Pin, GPIO_PIN_RESET);
#define LCD_RES_H                       //HAL_GPIO_WritePin(LCD_RES_GPIO_Port, LCD_RES_Pin, GPIO_PIN_SET);

//#define SSD1306_I2C_ADDRESS             0x3C  // 011110+SA0+RW - 0x3C or 0x3D
// Address for 128x32 is 0x3C
// Address for 128x64 is 0x3D (default) or 0x3C (if SA0 is grounded)

#define LCD_DAT_L                       GPIOB->BSRR = GPIO_BSRR_BR_15
#define LCD_DAT_H                       GPIOB->BSRR = GPIO_BSRR_BS_15
#define LCD_CLK_L                       GPIOB->BSRR = GPIO_BSRR_BR_13
#define LCD_CLK_H                       GPIOB->BSRR = GPIO_BSRR_BS_13
#define LCD_CS_L                        GPIOB->BSRR = GPIO_BSRR_BR_12
#define LCD_CS_H                        GPIOB->BSRR = GPIO_BSRR_BS_12

#endif


#if LCD_LM15SGFN07

#define COLORS_16BIT                    1

//#define BLACK 0
//#define WHITE 1
//#define INVERSE 2

#ifndef COLOR
#if COLORS_16BIT
typedef uint16_t                        COLOR;
#else
typedef uint8_t                         COLOR;
#endif
#endif

//#define LCD_ENABLE_PARTIAL_UPDATE   1
#define SCREEN_W	                    101
#define SCREEN_H	                    80
#define SCREEN_VIRTUAL_W                SCREEN_W
#define SCREEN_VIRTUAL_H                SCREEN_H

#define SCREEN_ORIENTATION_GLOBAL       SCREEN_ORIENTATION_0
#if (SCREEN_W > SCREEN_H)
    #define PAINT_BUF_SIZE              SCREEN_W
#else
    #define PAINT_BUF_SIZE              SCREEN_H
#endif
                     
#define LCD_CLK_L                       GPIOA->ODR &= ~GPIO_PIN_8
#define LCD_CLK_H                       GPIOA->ODR |=  GPIO_PIN_8

#define LCD_DAT_L                       GPIOB->ODR &= ~GPIO_PIN_12
#define LCD_DAT_H                       GPIOB->ODR |=  GPIO_PIN_12

#define LCD_RES_L                       GPIOB->ODR &= ~GPIO_PIN_14
#define LCD_RES_H                       GPIOB->ODR |=  GPIO_PIN_14

#define LCD_CS_L                        GPIOB->ODR &= ~GPIO_PIN_13 //HAL_GPIO_WritePin(GPIOB, LCD_CS_Pin, GPIO_PIN_RESET);
#define LCD_CS_H                        GPIOB->ODR |=  GPIO_PIN_13 //HAL_GPIO_WritePin(GPIOB, LCD_CS_Pin, GPIO_PIN_SET);

#define LCD_RS_L                        GPIOB->ODR &= ~GPIO_PIN_15 //HAL_GPIO_WritePin(GPIOB, LCD_RS_Pin, GPIO_PIN_RESET);
#define LCD_RS_H                        GPIOB->ODR |=  GPIO_PIN_15 //HAL_GPIO_WritePin(GPIOB, LCD_RS_Pin, GPIO_PIN_SET);

#endif


//------------------------------- UART -----------------------------------------
#define HAL_USART1                      1
#define USART1_TX_HOOK                  do {Uart1TX_Ready = SET;} while (0)
#define USART1_RX_HOOK                  do {Uart1RX_Ready = SET; ; } while (0)
#define USART1_TX_ERROR_HOOK            ERROR_ACTION(ERROR_USART1_HAL)
#define USART1_TBUF_SIZE                (128)
#define USART1_RBUF_SIZE                (512)
#define USART1_LED_INV                  //LED_BLUE_INV 

// 
#define HAL_USART2                      1
#define USART2_TX_HOOK                  do {Uart2TX_Ready = SET;} while (0)
#define USART2_RX_HOOK                  do {Uart2RX_Ready = SET; } while (0)
#define USART2_TX_ERROR_HOOK            ERROR_ACTION(ERROR_USART2_HAL)
#define USART2_BUF_TX_SIZE              (16)
#define USART2_BUF_RX_SIZE              (16)
#define USART2_LED_INV                  //LED_BLUE_INV 

//PB11-RX PB10-TX
#define HAL_USART3                      1
#define USART3_TX_HOOK                  do {Uart3TX_Ready = SET;} while (0)
#define USART3_RX_HOOK                  do {Uart3RX_Ready = SET; } while (0)
#define USART3_TX_ERROR_HOOK            ERROR_ACTION(ERROR_USART3_HAL)
#define USART3_TBUF_SIZE                (64)
#define USART3_RBUF_SIZE                (64)
#define USART3_LED_INV                  //LED_BLUE_INV 

//#define HAL_SPI1                        1


// for xprintf.h
#define _USE_XFUNC_OUT	                1 // 1: Use output functions
#define	_CR_CRLF		                0 // 1: Convert \n ==> \r\n in the output char
#define _USE_XFUNC_IN	                0 // 1: Use input function
#define	_LINE_ECHO		                0 // 1: Echo back input chars in xgets function

//for conv.h
#define STR_MAX_SIZE                    (65535 -1)

// for modRTC.h
//#define MOD_RTC                     1
//#define RTC_CORRECT_TIC             1
//#define RTC_NEED_CALENDAR           1
//#define RTC_NEED_ALARM              1

// vor _crc.h
//#define NEED_CRC32_CCITT        1 
#define NEED_CRC16                      1
//#define NEED_CRC8               1
//#define NEED_CRC8_DS            1


//#define __NOP()               
#define __DI()                          __disable_irq() // do { #asm("cli") } while(0) // Global interrupt disable
#define __EI()                          __enable_irq() //do { #asm("sei") } while(0) // Global interrupt enable
#define __CLRWDT()                      do { IWDG->KR = 0x0000AAAA; } while (0) // ������������ �������

#define show_message(a)                 show_message_on_LCD(a) //NULL
#define _show_message                   _show_message_on_LCD //NULL








#include "_fifo.h"

#define UART_BUF_RX_SIZE                128
//#define UART_BUF_TX_SIZE            UART_BUF_RX_SIZE




// I2C ---------------------------------
#define HAL_I2C1                        1

#define I2C_LIS302DL                    1
#define I2C_PCA8885                     1
#define I2C_BME280                      1
#define I2C_MAX30100                    1

#define I2C_LIS302DL_ADDRESS            0x1C //TODO

#define I2C_PCA8885_ADDRESS             0x20

#define I2C_FRAM_ADDRESS                0x50

#define I2C_BME280_ADDRESS              0x76

#define I2C_MAX30100_ADDRESS            (0x57<<1)


#define I2C1_SLEEP_TIME 			    20

#if (1 == HAL_I2C1)
    #define GPIO_I2C1                   GPIOB
    #define GPIO_PIN_SCL1               GPIO_PIN_6
    #define GPIO_PIN_SDA1               GPIO_PIN_7

    #define SCL1_L                      GPIO_I2C1->BSRR = GPIO_BSRR_BR_6 //I2C_GPIO->ODR &= ~GPIO_Pin_SCL                                
    #define SCL1_H                      GPIO_I2C1->BSRR = GPIO_BSRR_BS_6 //I2C_GPIO->ODR |=  GPIO_Pin_SCL

    #define SDA1_L                      GPIO_I2C1->BSRR = GPIO_BSRR_BR_7
    #define SDA1_H                      GPIO_I2C1->BSRR = GPIO_BSRR_BS_7

    #define SCL1_IN                     (GPIO_I2C1->IDR & GPIO_IDR_6)
    #define SDA1_IN                     (GPIO_I2C1->IDR & GPIO_IDR_7)

    #define I2C1_SLEEP_TIME             20

    // GPIO_Init(I2C_GPIO, (GPIO_Pin_TypeDef)GPIO_Pin_SDA, GPIO_MODE_OUT_OD_HIZ_FAST); \
    // GPIO_Init(I2C_GPIO, (GPIO_Pin_TypeDef)GPIO_Pin_SCL, GPIO_MODE_OUT_OD_HIZ_FAST); 
    #define I2C1_INIT    RCC->AHBENR |= RCC_AHBENR_GPIOBEN; \
        SCL1_H; \
        SDA1_H; \
        GPIO_I2C1->MODER &= ~(GPIO_MODER_MODER6 | GPIO_MODER_MODER7); \
        GPIO_I2C1->MODER |= (GPIO_MODER_MODER6_0 | GPIO_MODER_MODER7_0); \
        GPIO_I2C1->OTYPER |= (GPIO_OTYPER_OT_6 | GPIO_OTYPER_OT_7); \
        GPIO_I2C1->OSPEEDR |= (GPIO_OSPEEDR_OSPEEDR6 | GPIO_OSPEEDR_OSPEEDR7); \
        GPIO_I2C1->PUPDR &= ~(GPIO_PUPDR_PUPDR6 | GPIO_PUPDR_PUPDR7); //
    
    #define I2C1_DEINIT SDA1_H; SCL1_H
    #define I2C1_DELAY                  halI2C1_delay()
            
#define halI2C_init()                   soft_halI2C_init() 
#define halI2C_deinit()                 soft_halI2C_deinit() 
#define halI2C_transmit(a,b,c,d)        soft_halI2C_transmit(a,b,c,d) 
#define halI2C_receive(a,b,c,d)         soft_halI2C_receive(a,b,c,d) 

#endif

#if (2 == HAL_I2C2)
    #define GPIO_I2C1                   GPIOA
    #define GPIO_PIN_SCL1               GPIO_PIN_2
    #define GPIO_PIN_SDA1               GPIO_PIN_3

    #define SCL1_L                      GPIO_I2C1->BSRR = GPIO_BSRR_BR_2 //I2C_GPIO->ODR &= ~GPIO_Pin_SCL                                
    #define SCL1_H                      GPIO_I2C1->BSRR = GPIO_BSRR_BS_2 //I2C_GPIO->ODR |=  GPIO_Pin_SCL

    #define SDA1_L                      GPIO_I2C1->BSRR = GPIO_BSRR_BR_3
    #define SDA1_H                      GPIO_I2C1->BSRR = GPIO_BSRR_BS_3

    #define SCL1_IN                     (GPIO_I2C1->IDR & GPIO_IDR_2)
    #define SDA1_IN                     (GPIO_I2C1->IDR & GPIO_IDR_3)

    #define I2C1_INIT    RCC->AHBENR |= RCC_AHBENR_GPIOAEN; \
        SCL1_H; \
        SDA1_H; \
        GPIO_I2C1->MODER &= ~(GPIO_MODER_MODER3 | GPIO_MODER_MODER2); \
        GPIO_I2C1->MODER |= (GPIO_MODER_MODER3_0 | GPIO_MODER_MODER2_0); \
        GPIO_I2C1->OTYPER |= (GPIO_OTYPER_OT_3 | GPIO_OTYPER_OT_2); \
        GPIO_I2C1->OSPEEDR |= (GPIO_OSPEEDR_OSPEEDR3 | GPIO_OSPEEDR_OSPEEDR2); \
        GPIO_I2C1->PUPDR |= (GPIO_PUPDR_PUPDR3_0 | GPIO_PUPDR_PUPDR2_0);
    
    #define I2C1_DEINIT SDA1_H; SCL1_H
    #define I2C1_DELAY                  halI2C1_delay()
            
#define halI2C_init()                   soft_halI2C_init() 
#define halI2C_deinit()                 soft_halI2C_deinit()
#define halI2C_transmit(a,b,c,d)        soft_halI2C_transmit(a,b,c,d) 
#define halI2C_receive(a,b,c,d)         soft_halI2C_receive(a,b,c,d) 

#endif
        
        
//WIFI ---------------------------------
//#define DEFAULT_SSID					"85_card_sized_phone vA"
//#define DEFAULT_PASS					"11111111"
#define DEFAULT_SSID					"networkWIFI"
#define DEFAULT_PASS					"pass12345678"

        
// EEPROM ------------------------------
#define HAL_EEPROM_SIZE					512 //in bytes
#pragma pack(push, 1)
typedef struct EEPROM_config_ { //size must be < 4096 bytes
    char ssid [32]; //��� ����� �������
    char pass [32]; //������ ����� �������

    U8 OLED_contrast;

} EEPROM_config;

#pragma pack(pop) 


// Keys --------------------------------
enum { 
    HKEY_NOT_KEY = 0x00, 
    HKEY_0 = 0x41, 
    HKEY_1 = 0x18,
    HKEY_2 = 0x14,
    HKEY_3 = 0x12,
    HKEY_4 = 0x28,
    HKEY_5 = 0x24,
    HKEY_6 = 0x22,
    HKEY_7 = 0x48,
    HKEY_8 = 0x44,
    HKEY_9 = 0x42,
    HKEY_DOT = 0x21,
    HKEY_EQUAL = 0x11,
    HKEY_UP = 0x84,
    HKEY_DOWN = 0x81,
    HKEY_LEFT = 0x88,
    HKEY_RIGHT = 0x82

};


typedef enum KEY_BUTTONS_ { // ���������� ������
    KEY_0 = 0,
    KEY_1,
    KEY_2,
    KEY_3,
    KEY_4,
    KEY_5,
    KEY_6,
    KEY_7,
    KEY_8,
    KEY_9,
    KEY_DOT,
    KEY_EQUAL,
    KEY_UP,
    KEY_DOWN,
    KEY_LEFT,
    KEY_RIGHT,
    
    NUMBER_KEYS         // ���������� ������, �� �������!
} KEY_BUTTONS;

// Menu --------------------------------


#define MAKE_MENU(name, next, previous, child, parent, value, callback, text) \
    extern menuItem_t next; \
    extern menuItem_t previous; \
    extern menuItem_t child; \
    extern menuItem_t parent; \
    menuItem_t name = { \
    (void *)&next, \
    (void *)&previous, \
    (void *)&child, \
    (void *)&parent, \
    (void *)&value, \
    (void *)&callback, \
    text,}


#define NULL_ENTRY  Null_Menu
#define PREVIOUS   ((menuItem_t*)(selectedMenuItem->previous))
#define NEXT       ((menuItem_t*)(selectedMenuItem->next))
#define PARENT     ((menuItem_t*)(selectedMenuItem->parent))
#define CHILD      ((menuItem_t*)(selectedMenuItem->child))
//#define PROPERTUES ((selectedMenuItem->editable))
#define VALUE	   ((selectedMenuItem->value))
#define CALLBACK   ((selectedMenuItem->callback))


    

//for game worm
#define WORM_SIZE                       4
#define WORM_KEY_RIGHT                  KEY_6
#define WORM_KEY_LEFT                   KEY_4
#define WORM_KEY_DOWN                   KEY_2
#define WORM_KEY_UP                     KEY_8
#define WORM_KEY_OK                     KEY_1
#define COLOR_BODY                      COLOR_WHITE
#define COLOR_HEAD                      COLOR_WHITE
#define COLOR_FOOD                      COLOR_WHITE
#define WORM_MAX_SIZE                   64
    

#ifdef	__cplusplus
extern "C" {
#endif


#ifdef	__cplusplus
}
#endif

#endif	/* BOARD_H */
